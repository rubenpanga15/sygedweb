 create table `personne`(
    id int(11) unsigned auto_increment primary key,
    `nom` varchar(255) not null,
    `postNom` varchar(255) ,
	`prenom` varchar(255) not null,
    `lieuNaissance` varchar(255) not null,
    dateNaissance datetime not null default now(),
    `sexe` varchar(1) not null,
    telephone varchar(25),
    `email` varchar(255),
    `profession` varchar(255) not null,
	`etatCivil` varchar(255),
    `nomPere` varchar(255) not null,
    `nomMere` varchar(255) not null,
    `province` varchar(255),
    `district` varchar(255),
    `secteur` varchar(255) ,
    `groupement` varchar(255) ,
    `adresse` varchar(255) not null,
    statut tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table `document`(
    numero varchar(40) not null primary key,
    `numeroRef` varchar(25) not null,
    dateDocument datetime not null,
    fkTypeDocument int(11) unsigned not null,
    fkDeclaration varchar(25) not null,
    statut tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table `user`(
    id int(11) unsigned auto_increment primary key,
    `username` varchar(255) not null,
    `password` varchar(255) not null,
    `matricule` varchar(25) not null,
	`role` varchar(255) not null,
	`token`varchar(255),
    `fkCentre` varchar(25) not null,
	`fkPersonne` int(11) unsigned not null,
    statut tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table `declaration`(
    `numero` varchar(25) not null primary key,
    fkBeneficiaire int(11) unsigned not null,
    fkRequerant int(11) unsigned not null,
    `langue` varchar(255) not null,
    fkInterprete int(11) unsigned,
    fkRedacteur int(11) unsigned not null,
	`observationRedacteur` varchar(255) not null,
    fkPrepose int(11) unsigned,
    `observationPrepose` varchar(255),
    fkOec int(11) ,
    `observationOec` varchar(255) ,
    `avis` varchar(255) ,
	`type` varchar(255) not null,
    dateAvis datetime,
    statut tinyint(1) not null default 1,
    dateCreat datetime not null default now(),
	fkCentre varchar(25) not null
);

create table `acteNaissance`(
   `numero` varchar(25) not null primary key,
   `lieuNaissance` varchar(255) not null,
    dateNaissance datetime not null ,
    fkPere int(11) unsigned not null,
    fkMere int(11) unsigned not null,
	fkDeclaration varchar(25) not null,
    statut tinyint(1) not null default 1,
    fkCentre varchar(25) not null,
    agentCreat integer not null,
    dateCreat datetime not null default now(),
    isImprimer tinyint(1) not null default 0,
    agentImpression int(11) unsigned,
    dateImpressiom datetime
);

create table `acteDeces`(
    `numero` varchar(25) not null primary key,
    fkPere int(11) unsigned not null,
    fkMere int(11) unsigned not null,
    `lieuDeces` varchar(255) not null,
    circonstance varchar(255) not null,
    dateDeces datetime not null,
    etatCivil varchar(25) not null,
	fkDeclaration varchar(25) not null,
    statut tinyint(1) not null default 1,
    dateCreat datetime not null default now(),
    isImprimer tinyint(1) not null default 0,
    agentImpression int(11) unsigned,
    dateImpressiom datetime,
    fkCentre varchar(25) not null
);

create table `acteMariage` (
    `numero` varchar(11) not null primary key,
    `regimeMatrimonial` varchar(255) not null,
    montantDot decimal(11,3) not null,
    fkPereEpouse int(11) unsigned not null,
    fkMereEpouse int(11) unsigned not null,
    fkPereEpoux int(11) unsigned not null,
    fkMereEpoux int(11) unsigned not null,
    `lieuPublicite` varchar(255) not null,
    dateMariage datetime not null,
    agentCreat int(11) unsigned not null,
    fkDeclaration varchar(25) not null,
    statut tinyint(1) not null default 1,
    dateCreat datetime not null default now(),
    isImprimer tinyint(1) not null default 0,
    agentImpression int(11) unsigned,
    dateImpressiom datetime,
    fkCentre varchar(25) not null
);

create table `typeDocument`(
    id int(11) unsigned auto_increment primary key,
    `description` varchar(255) not null,
    aScanner tinyint(1) not null default 1,
    statut tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table `documentProcedure` (
    id int(11) unsigned auto_increment primary key,
    fkTypeDocument int(11) not null,
    isObligatoire tinyint(1) not null default 1,
    fkProcedure int(11) not null,
    statut tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table `procedure` (
    id int(11) unsigned auto_increment primary key,
    `description` varchar(255) not null,
    statut tinyint(1) not null default 1,
	`type` varchar(255) not null,
    isPayant tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table `lienArchive` (
	id int(11) unsigned auto_increment primary key,
	`path` varchar(255) not null,
	fkDocument varchar(40) not null,
	statut tinyint(1) not null default 1,
    dateCreat datetime not null default now()
);

create table `centre` (
	code varchar(25) primary key,
	description varchar(255) not null,
	statut tinyint(1) not null default 1,
	dateCreat datetime not null default now()
);

create table `site` (
	code varchar(25) primary key,
	description varchar(255) not null,
	statut tinyint(1) not null default 1,
	dateCreat datetime not null default now()
);

create table `province` (
	code varchar(25) primary key,
	description varchar(255) not null,
	statut tinyint(1) not null default 1,
	dateCreat datetime not null default now()
);

create table config (
    code varchar(30) not null primary key,
    url varchar(250),
    username varchar(50),
    token varchar(250),
    extra varchar(250),
    status tinyint(1) default 1
);

create table `procuration`(
   `numero` varchar(25) not null primary key,
   `interesse` varchar(255) not null,
    fkPere int(11) unsigned not null,
    fkMere int(11) unsigned not null,
	`enfant` varchar(255) not null,
	`situationMatrimoniale` varchar(255) not null,
	`divorceLe` varchar(255) not null,
	`mandataire` varchar(255) not null,
	`personneContact` varchar(255) not null
  
);

create table `procurationNaissance`(
   `numero` varchar(25) not null primary key,
   `interesse` int(11) unsigned not null,
    fkPere int(11) unsigned not null,
    fkMere int(11) unsigned not null,
	fkEnfant int(11) unsigned not null,
	`situationMatrimoniale` varchar(255) not null,
	`divorceLe` datetime,
	`mandataire` int(11) unsigned not null,
    preuvePaiement varchar not null,
	`personneContact` int(11) unsigned not null,
    dateCreat datetime not null default now(),
    isImprimer tinyint(1) not null default 0,
    agentImpression int(11) unsigned,
    dateImpressiom datetime,
    agentCreat int(11) unsigned not null,
    fkCentre varchar(25) not null,
    statut tinyint(1) not null default 1
);

create table `procurationDeces`(
   `numero` varchar(25) not null primary key,
   `interesse` int(11) unsigned not null,
    fkPere int(11) unsigned not null,
    fkMere int(11) unsigned not null,
	fkDefunt int(11) unsigned not null,
	`divorceLe` datetime,
    preuvePaiement varchar not null,
	`mandataire` int(11) unsigned not null,
    dateCreat datetime not null default now(),
    isImprimer tinyint(1) not null default 0,
    agentImpression int(11) unsigned,
    dateImpressiom datetime,
    agentCreat int(11) unsigned not null,
    fkCentre varchar(25) not null,
    statut tinyint(1) not null default 1
);

create table `attestation`(
   `numero` varchar(25) not null primary key,
   `lieunaissance` varchar(255) not null,
   `datenaissance` varchar(255) not null,
    fkPere int(11) unsigned not null,
    fkMere int(11) unsigned not null,
	fkCentre varchar(255) not null,
    agentCreat int(11) unsigned not null
);

create table `attestationNaissance`(
   `numero` varchar(25) not null primary key,
   `lieuNaissance` varchar(255) not null,
   `dateNaissance` datetime not null,
    fkPere int(11) unsigned not null,
    fkEnfant int(11) unsigned not null,
    fkMere int(11) unsigned not null,
	fkCentre varchar(255) not null,
    preuvePaiement varchar not null,
    agentCreat int(11) unsigned not null,
    statut tinyint(1) not null default 1,
    dateCreat datetime not null default now(),
    isImprimer tinyint(1) not null default 0,
    agentImpression int(11) unsigned,
    dateImpressiom datetime
);
