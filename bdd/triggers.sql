create trigger tr_declaration on declaration before insert
for each row
begin
    declare exist integer;

    if new.numero is null or (new.numero is not null and length(new.numero)<=0) then
        
        select count(*) into exist from declaration where right(year(dateCreat), 2)=right(year(now()), 2) and fkCentre=new.fkCentre;

        if exist is null or exist=0 then
            set exist = 1;
        else
            set exist = exist + 1;
        end if;

        set new.numero = concat('DD', new.fkCentre, lpad(exist,5,0), right(year(now()), 2));
    end if;
end/

create trigger tr_acte_naissance on acteNaissance before insert
for each row
begin
    declare exist integer;

    if new.numero is null or (new.numero is not null and length(new.numero)<=0) then
        
        select count(*) into exist from acteNaissance where right(year(dateCreat), 2)=right(year(now()), 2) and fkCentre=new.fkCentre;

        if exist is null or exist=0 then
            set exist = 1;
        else
            set exist = exist + 1;
        end if;

        set new.numero = concat('AN', new.fkCentre, lpad(exist,5,0), right(year(now()), 2));
    end if;
end/

create trigger tr_acte_deces on acteDeces before insert
for each row
begin
    declare exist integer;

    if new.numero is null or (new.numero is not null and length(new.numero)<=0) then
        
        select count(*) into exist from acteNaissance where right(year(dateCreat), 2)=right(year(now()), 2) and fkCentre=new.fkCentre;

        if exist is null or exist=0 then
            set exist = 1;
        else
            set exist = exist + 1;
        end if;

        set new.numero = concat('AD', new.fkCentre, lpad(exist,5,0), right(year(now()), 2));
    end if;
end/

create trigger tr_acte_mariage on acteMariage before insert
for each row
begin
    declare exist integer;

    if new.numero is null or (new.numero is not null and length(new.numero)<=0) then
        
        select count(*) into exist from acteMariage where right(year(dateCreat), 2)=right(year(now()), 2) and fkCentre=new.fkCentre;

        if exist is null or exist=0 then
            set exist = 1;
        else
            set exist = exist + 1;
        end if;

        set new.numero = concat('AM', new.fkCentre, lpad(exist,5,0), right(year(now()), 2));
    end if;
end/

create trigger tr_procuration_naissance on procurationNaissance before insert
for each row
begin
    declare exist integer;

    if new.numero is null or (new.numero is not null and length(new.numero)<=0) then
        
        select count(*) into exist from procurationNaissance where right(year(dateCreat), 2)=right(year(now()), 2) and fkCentre=new.fkCentre;

        if exist is null or exist=0 then
            set exist = 1;
        else
            set exist = exist + 1;
        end if;

        set new.numero = concat('PN', new.fkCentre, lpad(exist,5,0), right(year(now()), 2));
    end if;
end/

create trigger tr_procuration_deces on procurationDeces before insert
for each row
begin
    declare exist integer;

    if new.numero is null or (new.numero is not null and length(new.numero)<=0) then
        
        select count(*) into exist from procurationDeces where right(year(dateCreat), 2)=right(year(now()), 2) and fkCentre=new.fkCentre;

        if exist is null or exist=0 then
            set exist = 1;
        else
            set exist = exist + 1;
        end if;

        set new.numero = concat('PD', new.fkCentre, lpad(exist,5,0), right(year(now()), 2));
    end if;
end/

create trigger tr_attestation_naissance on attestationNaissance before insert
for each row
begin
    declare exist integer;

    if new.numero is null or (new.numero is not null and length(new.numero)<=0) then
        
        select count(*) into exist from attestationNaissance where right(year(dateCreat), 2)=right(year(now()), 2) and fkCentre=new.fkCentre;

        if exist is null or exist=0 then
            set exist = 1;
        else
            set exist = exist + 1;
        end if;

        set new.numero = concat('ATN', new.fkCentre, lpad(exist,5,0), right(year(now()), 2));
    end if;
end/