/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import contracts.IConfigurationController;
import contracts.IDeclaration;
import daos.DaoAttestation;
import daos.DaoDeclaration;
import daos.DaoProcuration;
import entities.Actedeces;
import entities.Actemariage;
import entities.Actenaissance;
import entities.Attestation;
import entities.AttestationNaissance;
import entities.Config;
import entities.Declaration;
import entities.Document;
import entities.Documentprocedure;
import entities.ErrorResponse;
import entities.Lienarchive;
import entities.Personne;
import entities.Procuration;
import entities.ProcurationDeces;
import entities.ProcurationNaissance;
import entities.Typedocument;
import entities.User;
import entities.ValueDataException;
import entities.utilities.ActeDecesRequest;
import entities.utilities.ActeMariageRequest;
import entities.utilities.ActeNaissanceRequest;
import entities.utilities.AttestationRequest;
import entities.utilities.DeclarationAdapter;
import entities.utilities.DocumentRequest;
import entities.utilities.Quittance;
import entities.utilities.UploadFileForDeclarationRequestItem;
import entities.utilities.UploadFileForDeclarationResponse;
import entities.utilities.UplodeFileForDeclarationRequest;
import http.HttpDataResponse;
import http.HttpQuery;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.json.JSONObject;
import others.AppUtilities;
import others.Logger;
import servlets.SaveProcurationApi;
import servlets.SaveaAttestationApi;
import utilities.AppConst;

/**
 *
 * @author AdolphTrinix
 */
public class DeclarationController implements IDeclaration {

    private static final String TAG = DeclarationController.class.getSimpleName();

    private ErrorResponse error;

    public DeclarationController() {
        this.error = new ErrorResponse();
    }

    public ErrorResponse getError() {
        return error;
    }

    @Override
    public Declaration createDeclation(String centre, Integer fkBeneficiaire, Integer fkRequerant, String langue, Integer fkInterprete, Integer fkRedacteur, String observationRedacteur, String type, String refPaiement, int userId, Object acte, List<DocumentRequest> documentsRequest) {
        Declaration obj = null;
        try {

            AppUtilities.controlValue(centre, "Renseignez le centre de déclaration");
            AppUtilities.controlValue(fkBeneficiaire, "Veuillez Renseignez l'identatfiant du beneficiaire");
            AppUtilities.controlValue(fkRequerant, "Veuillez Renseignez l'identifiant du requerant");
            AppUtilities.controlValue(fkRedacteur, "Veuillez Renseignez l'identifiant du redacteur");

            AppUtilities.controlValue(langue, "Veuillez Renseignez la langue dans laquelle la declaration a ete faite");
            AppUtilities.controlValue(fkInterprete, "Veuillez Renseignez l identifiant de de l interprete ");

            AppUtilities.controlValue(type, "Veuillez Renseignez le type");
            AppUtilities.controlValue(userId, "Veuillez renseignez l'utilisateur");

            if (observationRedacteur == null) {
                observationRedacteur = "";
            }

            DaoDeclaration dao = new DaoDeclaration();

            List<Documentprocedure> documentsProcedure = dao.getDocumentAArchiver(type);
            List<Document> documents = null;

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

            if (documentsProcedure != null) {
                AppUtilities.controlValue(documentsRequest, "Aucun document n'a été fournit");

                for (Documentprocedure dp : documentsProcedure) {
                    if (dp.getIsObligatoire()) {
                        for (DocumentRequest dr : documentsRequest) {
                            if ((int) dp.getFkTypeDocument() == (int) dr.getTypeDocument()) {
                                AppUtilities.controlValue(dr.getDateDocument(), dp.getTypeDocument().getDescription() + " - Renseignez la date du document");
                                AppUtilities.controlValue(dr.getNumeroRef(), dp.getTypeDocument().getDescription() + " - Renseignez le numéro du document");
                                AppUtilities.controlValue(dr.getTypeDocument(), "Certains document n'ont pas de type document");
                                dr.setDateDocument(AppUtilities.convertStringDateToYyyyMMdd(dr.getDateDocument()));
                                AppUtilities.controlValue(dr.getDateDocument(), "Le format de la date du document n'est pas correct : yyyyMMdd");

                                if (documents == null) {
                                    documents = new ArrayList();
                                }

                                Document document = new Document();
                                document.setDateCreat(Calendar.getInstance().getTime());
                                document.setDateDocument(sdf.parse(dr.getDateDocument()));
                                document.setFkTypeDocument(dr.getTypeDocument());
                                document.setNumeroRef(dr.getNumeroRef());
                                document.setStatut(true);

                                documents.add(document);
                            }
                        }
                    }
                }
            }

            obj = new Declaration();

            obj.setFkBeneficiaire(fkBeneficiaire);
            obj.setFkInterprete(fkInterprete);
            obj.setFkRedacteur(fkRedacteur);
            obj.setFkRequerant(fkRequerant);
            obj.setLangue(langue);
            obj.setType(type);
            obj.setDateCreat(Calendar.getInstance().getTime());
            obj.setStatut(true);
            obj.setNumero("");
            obj.setFkCentre(centre);

            obj.setNumero(dao.saveDeclaration(obj));
            AppUtilities.controlValue(obj.getNumero(), "Echec lors de la création de la déclaration");

            if (documents != null) {
                for (int i = 0; i < documents.size(); i++) {
                    Document document = documents.get(i);

                    document.setFkDeclaration(obj.getNumero());
                    document.setNumero(obj.getNumero() + "" + (i + 1));

                    AppUtilities.controlValue(dao.saveReturnCode(document), "Echec lors de la création du document");
                }
            }

            String numero = null;

            switch (type.toUpperCase()) {
                case Declaration.TYPE_ACTE_NAISSANCE:
                    ActeNaissanceRequest acteNaissance = (ActeNaissanceRequest) acte;
                    numero = this.saveActeNaissance(centre, obj.getNumero(), acteNaissance.getLieuNaissance(), acteNaissance.getDateNaissance(), acteNaissance.getPere(), acteNaissance.getMere(), refPaiement, userId);

                    break;
                case Declaration.TYPE_ACTE_DECES:
                    ActeDecesRequest acteDeces = (ActeDecesRequest) acte;
                    numero = this.saveActeDeces(centre, obj.getNumero(), acteDeces.getLieuDeces(), acteDeces.getDateDeces(), acteDeces.getPere(), acteDeces.getMere(), acteDeces.getCirconstance(), refPaiement, userId, acteDeces.getEtatCivil());
                    break;
                case Declaration.TYPE_ACTE_MARIAGE:
                    ActeMariageRequest acteMariage = (ActeMariageRequest) acte;
                    numero = this.saveActeMariage(centre, obj.getNumero(), acteMariage.getLieuPublicite(), acteMariage.getDateMariage(), acteMariage.getPereEpoux(), acteMariage.getMereEpoux(), acteMariage.getPereEpouse(), acteMariage.getMereEpouse(), acteMariage.getRegimeMatrimonial(), refPaiement, userId);
                    break;
                default:
                    throw new ValueDataException("Ce type d'acte n'est pas pris en charge");
            }

            AppUtilities.controlValue(numero, error.getErrorDescription());

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("Déclaration enregistrée avec succès");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return obj;
    }

    private String saveActeNaissance(String centre, String numeroDeclaration, String lieuNaissance, String dateNaissance, Integer fkPere, Integer fkMere, String refPaiement, int userId) {
        error = new ErrorResponse();
        String numero = null;

        try {
            AppUtilities.controlValue(centre, "Renseignez le centre de déclaration de l'acte de naissance");
            AppUtilities.controlValue(numeroDeclaration, "Aucun numéro de déclaration fournit");

            AppUtilities.controlValue(fkMere, "L'identité de la mère n'a pas été renseignée");
            AppUtilities.controlValue(fkPere, "L'identité du père n'a pas été renseignée");
            AppUtilities.controlValue(lieuNaissance, "Renseignez le lieu de naissance");
            AppUtilities.controlValue(dateNaissance, "Renseignez la date de naissance");
            dateNaissance = AppUtilities.convertStringDateToYyyyMMdd(dateNaissance);
            AppUtilities.controlValue(dateNaissance, "Le format de la date de naissance n'est pas correct : yyyyMMdd");

            if (refPaiement != null) {
                refPaiement = refPaiement.trim();
            }

            IConfigurationController ctrl = new ConfigurationController();
            Config config = ctrl.getConfig("AN_ECHEANCE");
            if (config == null) {
                throw new ValueDataException(ctrl.getError().getErrorDescription());
            }

            AppUtilities.controlValue(config.getExtra(), "Echéance de déclaration d'une naissance non configurée");

            Integer nbrJourEcheance = null;

            try {
                nbrJourEcheance = Integer.valueOf(config.getExtra().trim());
            } catch (Exception e) {
                e.printStackTrace();
                throw new ValueDataException("Format incorrecte de l'échéance de déclaration d'une naissance");
            }

            Actenaissance obj = new Actenaissance();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

            obj.setDateCreat(Calendar.getInstance().getTime());
            obj.setDateNaissance(sdf.parse(dateNaissance));
            obj.setFkDeclaration(numeroDeclaration);
            obj.setFkMere(fkMere);
            obj.setFkPere(fkPere);
            obj.setLieuNaissance(lieuNaissance);
            obj.setFkCentre(centre);
            obj.setAgentCreat(userId);
            obj.setStatut(true);
            obj.setNumero("");

            DaoDeclaration dao = new DaoDeclaration();

            Calendar dateEcheance = AppUtilities.getDateForJour(nbrJourEcheance, obj.getDateNaissance());
            if (dateEcheance.after(Calendar.getInstance().getTime())) {
                AppUtilities.controlValue(refPaiement, "Renseignez le numéro de la référence de paiement");

                Quittance quittance = verifierPreuvePaiement(refPaiement);
                if (quittance == null) {
                    throw new ValueDataException(error.getErrorDescription());
                }

                if (!quittance.getStatus()) {
                    throw new ValueDataException("Cette preuve de paiement a été annulée");
                }

                if (quittance.getUsed()) {
                    throw new ValueDataException("Cette preuve de paiement a déjà été utilisée pour un autre document");
                }

                if (dao.quittanceIsLocallyUsed(quittance.getNumero())) {
                    throw new ValueDataException("Cette preuve de paiement a déjà été utilisée dans un acte");
                }

            }

            obj.setNumero(dao.saveActeNaissance(obj));
            AppUtilities.controlValue(obj.getNumero(), "Echec lors de la création de l'acte de naissance");

            if (refPaiement != null && refPaiement.length() > 0) {
                dao.pushPreuvePaiement(numeroDeclaration, refPaiement);
            }

            numero = obj.getNumero();

            error.setErrorCode(ErrorResponse.OK);
            error.setErrorDescription("Acte de naissance créé avec succès");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return numero;
    }

    private String saveActeDeces(String centre, String numeroDeclaration, String lieuDeces, String dateDeces, Integer fkPere, Integer fkMere, String circonstance, String refPaiement, int userId, String etatCivil) {
        error = new ErrorResponse();
        String numero = null;

        try {
            AppUtilities.controlValue(centre, "Renseignez le centre de déclaration de l'acte de naissance");
            AppUtilities.controlValue(numeroDeclaration, "Aucun numéro de déclaration fournit");

            AppUtilities.controlValue(fkMere, "L'identité de la mère n'a pas été renseignée");
            AppUtilities.controlValue(fkPere, "L'identité du père n'a pas été renseignée");
            AppUtilities.controlValue(lieuDeces, "Renseignez le lieu du deces");
            AppUtilities.controlValue(dateDeces, "Renseignez la date du deces");
            dateDeces = AppUtilities.convertStringDateToYyyyMMdd(dateDeces);
            AppUtilities.controlValue(dateDeces, "Le format de la date du deces n'est pas correct : yyyyMMdd");
            AppUtilities.controlValue(etatCivil, "Veuillez renseigner l'état civil");

            if (refPaiement != null) {
                refPaiement = refPaiement.trim();
            }

            IConfigurationController ctrl = new ConfigurationController();
            Config config = ctrl.getConfig("AD_ECHEANCE");
            if (config == null) {
                throw new ValueDataException(ctrl.getError().getErrorDescription());
            }

            AppUtilities.controlValue(config.getExtra(), "Echéance de déclaration d'une naissance non configurée");

            Integer nbrJourEcheance = null;

            try {
                nbrJourEcheance = Integer.valueOf(config.getExtra().trim());
            } catch (Exception e) {
                e.printStackTrace();
                throw new ValueDataException("Format incorrecte de l'échéance de déclaration d'une naissance");
            }

            Actedeces obj = new Actedeces();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

            obj.setDateCreat(Calendar.getInstance().getTime());
            obj.setDateDeces(sdf.parse(dateDeces));
            obj.setFkDeclaration(numeroDeclaration);
            obj.setFkMere(fkMere);
            obj.setFkPere(fkPere);
            obj.setLieuDeces(lieuDeces);
            obj.setFkCentre(centre);
            obj.setAgentCreat(userId);
            obj.setCirconstance(circonstance);
            obj.setStatut(true);
            obj.setEtatCivil(etatCivil);
            obj.setNumero("");

            DaoDeclaration dao = new DaoDeclaration();

            Calendar dateEcheance = AppUtilities.getDateForJour(nbrJourEcheance, obj.getDateDeces());
            if (dateEcheance.after(Calendar.getInstance().getTime())) {
                AppUtilities.controlValue(refPaiement, "Renseignez le numéro de la référence de paiement");

                Quittance quittance = verifierPreuvePaiement(refPaiement);
                if (quittance == null) {
                    throw new ValueDataException(error.getErrorDescription());
                }

                if (!quittance.getStatus()) {
                    throw new ValueDataException("Cette preuve de paiement a été annulée");
                }

                if (quittance.getUsed()) {
                    throw new ValueDataException("Cette preuve de paiement a déjà été utilisée pour un autre document");
                }

                if (dao.quittanceIsLocallyUsed(quittance.getNumero())) {
                    throw new ValueDataException("Cette preuve de paiement a déjà été utilisée dans un acte");
                }

            }

            obj.setNumero(dao.saveActeDeces(obj));
            AppUtilities.controlValue(obj.getNumero(), "Echec lors de la création de l'acte de deces");

            if (refPaiement != null && refPaiement.length() > 0) {
                dao.pushPreuvePaiement(numeroDeclaration, refPaiement);
            }

            numero = obj.getNumero();

            error.setErrorCode(ErrorResponse.OK);
            error.setErrorDescription("Acte de deces créé avec succès");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return numero;
    }

    private String saveActeMariage(String centre, String numeroDeclaration, String lieuPublicite, String dateMariage, Integer fkPereEpoux, Integer fkMereEpoux, Integer fkPereEpouse, Integer fkMereEpouse, String regimeMatrimonial, String refPaiement, int userId) {
        error = new ErrorResponse();
        String numero = null;

        try {
            AppUtilities.controlValue(centre, "Renseignez le centre de déclaration de l'acte de naissance");
            AppUtilities.controlValue(numeroDeclaration, "Aucun numéro de déclaration fournit");

            AppUtilities.controlValue(fkMereEpoux, "L'identité de la mère n'a pas été renseignée");
            AppUtilities.controlValue(fkMereEpouse, "L'identité de la mère n'a pas été renseignée");
            AppUtilities.controlValue(fkPereEpoux, "L'identité du père n'a pas été renseignée");
            AppUtilities.controlValue(fkPereEpouse, "L'identité du père n'a pas été renseignée");
            AppUtilities.controlValue(lieuPublicite, "Renseignez le lieu du deces");
            AppUtilities.controlValue(dateMariage, "Renseignez la date du deces");
            dateMariage = AppUtilities.convertStringDateToYyyyMMdd(dateMariage);
            AppUtilities.controlValue(dateMariage, "Le format de la date du deces n'est pas correct : yyyyMMdd");

            if (refPaiement != null) {
                refPaiement = refPaiement.trim();
            }

            IConfigurationController ctrl = new ConfigurationController();
            Config config = ctrl.getConfig("AM_ECHEANCE");
            if (config == null) {
                throw new ValueDataException(ctrl.getError().getErrorDescription());
            }

            AppUtilities.controlValue(config.getExtra(), "Echéance de déclaration d'un mariage non configurée");

            Integer nbrJourEcheance = null;

            try {
                nbrJourEcheance = Integer.valueOf(config.getExtra().trim());
            } catch (Exception e) {
                e.printStackTrace();
                throw new ValueDataException("Format incorrecte de l'échéance de déclaration d'un mariage");
            }

            Actemariage obj = new Actemariage();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

            obj.setDateCreat(Calendar.getInstance().getTime());
            obj.setDateMariage(sdf.parse(dateMariage));
            obj.setFkDeclaration(numeroDeclaration);
            obj.setFkMereEpoux(fkMereEpoux);
            obj.setFkPereEpoux(fkPereEpoux);
            obj.setFkMereEpouse(fkMereEpouse);
            obj.setFkPereEpouse(fkPereEpouse);
            obj.setLieuPublicite(lieuPublicite);
            obj.setFkCentre(centre);
            obj.setAgentCreat(userId);
            obj.setRegimeMatrinonal(regimeMatrimonial);
            obj.setStatut(true);
            obj.setNumero("");

            DaoDeclaration dao = new DaoDeclaration();

            Calendar dateEcheance = AppUtilities.getDateForJour(nbrJourEcheance, obj.getDateMariage());
            if (dateEcheance.after(Calendar.getInstance().getTime())) {
                AppUtilities.controlValue(refPaiement, "Renseignez le numéro de la référence de paiement");

                Quittance quittance = verifierPreuvePaiement(refPaiement);
                if (quittance == null) {
                    throw new ValueDataException(error.getErrorDescription());
                }

                if (!quittance.getStatus()) {
                    throw new ValueDataException("Cette preuve de paiement a été annulée");
                }

                if (quittance.getUsed()) {
                    throw new ValueDataException("Cette preuve de paiement a déjà été utilisée pour un autre document");
                }

                if (dao.quittanceIsLocallyUsed(quittance.getNumero())) {
                    throw new ValueDataException("Cette preuve de paiement a déjà été utilisée dans un acte");
                }

            }

            obj.setNumero(dao.saveActeMariage(obj));
            AppUtilities.controlValue(obj.getNumero(), "Echec lors de la création de l'acte de mariage");

            if (refPaiement != null && refPaiement.length() > 0) {
                dao.pushPreuvePaiement(numeroDeclaration, refPaiement);
            }

            numero = obj.getNumero();

            error.setErrorCode(ErrorResponse.OK);
            error.setErrorDescription("Acte de deces créé avec succès");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return numero;
    }

    @Override
    public List<Declaration> getAllDeclaration() {
        List<Declaration> list = null;
        try {

            DaoDeclaration dao = new DaoDeclaration();
            list = dao.getAllDeclarat();

            if (list == null) {
                throw new ValueDataException("l'opération n'a pas abouti");
            }
            this.error.setErrorCode(ErrorResponse.OK);
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        return list;
    }

    @Override
    public Declaration getDeclaration(String numero) {
        Declaration declaration = null;

        try {
            AppUtilities.controlValue(numero, "Veuillez rensienger le numero de la déclaration");

            DaoDeclaration dao = new DaoDeclaration();

            declaration = dao.getDeclaration(numero);

            if (declaration == null) {
                throw new ValueDataException("Aucune déclaration retrouvée");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("Déclaration retrouvée");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {

            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return declaration;
    }

    @Override
    public Boolean deleteDeclaration(String numero) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Declaration updateDeclaration(String numero, Integer fkBeneficiaire, Integer fkRequerant, String langue, Integer fkInterprete, Integer fkRedacteur, String observationRedacteur, Integer fkPrepose, String observationPrepose, Integer fkOec, String observationOec, String avis, String type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Documentprocedure> getDocumentAArchiver(String typeActe) {
        List<Documentprocedure> list = null;
        error = new ErrorResponse();

        try {
            AppUtilities.controlValue(typeActe, "Renseignez le type d'acte");

            DaoDeclaration dao = new DaoDeclaration();

            list = dao.getDocumentAArchiver(typeActe);
            AppUtilities.controlValue(list, "Aucun document configuré pour l'archivage");

            error.setErrorCode(ErrorResponse.OK);
            error.setErrorDescription(list.size() + " document(s) configuré par l'archivage");

        } catch (ValueDataException e) {
            error.setErrorCode(ErrorResponse.KO);
            error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            error.setErrorCode(ErrorResponse.KO);
            error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    @Override
    public List<Lienarchive> saveDocumentLink(List<String> base64FileContent, String numeroDeclaration, Integer typeDocumentId, Integer userId) {
        error = new ErrorResponse();
        List<Lienarchive> list = null;

        try {
            AppUtilities.controlValue(base64FileContent, "Aucun document reçu");
            AppUtilities.controlValue(numeroDeclaration, "Renseignez le numéro de la déclaration");
            AppUtilities.controlValue(typeDocumentId, "Renseignez le type du document");
            AppUtilities.controlValue(userId, "Aucun utilisateur renseigné");

            IConfigurationController ctrl = new ConfigurationController();
            Config config = ctrl.getConfig("DECLARATION_ARCHIVE");
            if (config == null) {
                throw new ValueDataException(ctrl.getError().getErrorDescription());
            }

            DaoDeclaration dao = new DaoDeclaration();

            Declaration declaration = (Declaration) dao.findByNumero(Declaration.class, numeroDeclaration);
            if (declaration == null) {
                throw new ValueDataException("Cette déclaration n'existe pas");
            }

            Typedocument typeDocument = (Typedocument) dao.findById(Typedocument.class, typeDocumentId);
            if (typeDocument == null) {
                throw new ValueDataException("Le type de document renseigné n'existe pas");
            }

            Document obj = dao.getDocument(numeroDeclaration, typeDocumentId);
            if (obj == null) {
                throw new ValueDataException("Le document (" + typeDocument.getDescription() + ") n'existe pas dans cette déclaration");
            }

            list = dao.getLienArchiveForDocument(obj.getNumero());

            int nbr = 0;

            if (list != null) {
                nbr = list.size();
            }

            int errorExist = 0;

            for (int i = 0; i < base64FileContent.size(); i++) {
                String file = base64FileContent.get(i);

                Lienarchive lien = new Lienarchive();

                String fileName = "DA_" + numeroDeclaration + "" + typeDocumentId + "_" + (nbr + (i + 1)) + ".jpg";
                String pathFile = config.getUrl() + fileName;

                try {
                    File f = new File(pathFile);
                    if (f.exists()) {
                        throw new ValueDataException("Un fichier portant le même nom a été trouvé (" + fileName + ")");
                    }

                    f.createNewFile();

                    FileWriter fw = new FileWriter(f);

                    fw.write(file);
                    fw.flush();
                    fw.close();

                } catch (Exception e) {
                    e.printStackTrace();

                    throw new ValueDataException("Erreur lors de la création du fichier");
                }

                lien.setDateCreat(Calendar.getInstance().getTime());
                lien.setFkDocument(obj.getNumero());
                lien.setPath(pathFile);
                lien.setStatut(true);

                lien.setId(dao.save(lien));
                if (lien.getId() == null || (lien.getId() != null && ((int) lien.getId()) <= 0)) {
                    errorExist++;
                }

                if (list == null) {
                    list = new ArrayList();
                }

                list.add(lien);
            }

            if (errorExist == list.size()) {
                error.setErrorCode(ErrorResponse.KO);
            } else {
                error.setErrorCode(ErrorResponse.OK);
            }

            if (errorExist > 0) {
                error.setErrorDescription(errorExist + " erreur(s) sur un total de " + list.size());
            } else {
                error.setErrorDescription("Enregistrement des archives effectué avec succès");
            }

        } catch (ValueDataException e) {
            error.setErrorCode(ErrorResponse.KO);
            error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            error.setErrorCode(ErrorResponse.KO);
            error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    @Override
    public List<UploadFileForDeclarationResponse> saveDocumentLink(List<UploadFileForDeclarationRequestItem> items, int userId) {
        error = new ErrorResponse();
        List<UploadFileForDeclarationResponse> list = null;

        try {
            AppUtilities.controlValue(userId, "Renseignez l'utilisateur");
            AppUtilities.controlValue(items, "Aucune archive n'a été envoyée");

            int errors = 0;

            for (UploadFileForDeclarationRequestItem obj : items) {

                List<Lienarchive> liens = this.saveDocumentLink(obj.getFiles(), obj.getNumeroDeclaration(), obj.getTypeDocument(), userId);

                if (liens == null) {
                    throw new ValueDataException(error.getErrorDescription());
                }

                UploadFileForDeclarationResponse o = new UploadFileForDeclarationResponse();
                o.setFilesId(new ArrayList());
                o.setNumeroDeclaration(obj.getNumeroDeclaration());
                o.setTypeDocument(obj.getTypeDocument());

                for (Lienarchive lien : liens) {
                    o.getFilesId().add(lien.getId());
                }

                if (list == null) {
                    list = new ArrayList();
                }

                list.add(o);
            }

            if (errors > 0) {
                error.setErrorCode(ErrorResponse.OK);
                error.setErrorDescription("Y a eu quelques erreurs");
            } else {
                error.setErrorCode(ErrorResponse.OK);
                error.setErrorDescription("Enregistrement effectué avec succès");
            }

        } catch (ValueDataException e) {
            error.setErrorCode(ErrorResponse.KO);
            error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            error.setErrorCode(ErrorResponse.KO);
            error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public Quittance verifierPreuvePaiement(String numero) {
        Quittance obj = null;
        error = new ErrorResponse();

        try {
            AppUtilities.controlValue(numero, "Renseignez la référence du document");

            ConfigurationController ctrl = new ConfigurationController();

            Config config = ctrl.getConfig("VER_EC");
            if (config == null) {
                throw new ValueDataException(ctrl.getError().getErrorDescription());
            }

            AppUtilities.controlValue(config.getUrl(), "L'url du système externe n'est pas configurée");
            AppUtilities.controlValue(config.getUsername(), "Le login du système externe n'est pas configurée");
            AppUtilities.controlValue(config.getToken(), "Le token du système externe n'est pas configurée");

            if (!config.getStatus()) {
                throw new ValueDataException("L'accès au système externe a été désactivé");
            }

            JSONObject jsonQuery = new JSONObject();

            jsonQuery.put("username", config.getUsername());
            jsonQuery.put("token", config.getToken());
            jsonQuery.put("numero", numero);

            HttpURLConnection urlConnexion = HttpQuery.postRequest(config.getUrl(), jsonQuery.toString());
            Logger.printLog(TAG, config.getUrl());
            Logger.printLog(TAG, jsonQuery.toString());

            int status = urlConnexion.getResponseCode();
            Logger.printLog(TAG, "status http : " + status);

            HttpDataResponse<Quittance> httpDataResponse = null;

            switch (status) {
                case 201:
                case 200:
                    String jsonResponse = HttpQuery.getResponse(urlConnexion);
                    AppUtilities.controlValue(jsonResponse, "Contacte impossible avec le système externe");
                    Logger.printLog(TAG, jsonResponse);

                    httpDataResponse = new Gson().fromJson(jsonResponse, new TypeToken<HttpDataResponse<Quittance>>() {
                    }.getType());

                    break;
                default:
                    throw new ValueDataException("Impossible de se connecter au système externe eCollect");
            }

            if (httpDataResponse.getResponse() == null) {
                throw new ValueDataException(httpDataResponse.getError().getErrorDescription());
            }

            obj = httpDataResponse.getResponse();

            error = httpDataResponse.getError();

        } catch (ValueDataException e) {
            error.setErrorCode(ErrorResponse.KO);
            error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            error.setErrorCode(ErrorResponse.KO);
            error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return obj;
    }

    @Override
    public List<Declaration> getDeclarationByPeriode(String debut, String fin, String centre) {

        List<Declaration> list = new ArrayList<>();
        try {
            AppUtilities.controlValue(debut, "Veuillez renseigner la date de debut");

            DaoDeclaration dao = new DaoDeclaration();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            if (fin == null) {
                fin = debut;
            }

            Date dateDebut = sdf.parse(debut);
            Date dateFin = sdf.parse(fin);

            Calendar cal = Calendar.getInstance();
            cal.setTime(dateFin);

            cal.add(Calendar.HOUR_OF_DAY, 24 - cal.get(Calendar.HOUR_OF_DAY));

            dateFin = cal.getTime();

            list = dao.getDeclaration(dateDebut, dateFin);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException(AppConst.EMPTY_LIST);
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " declaration(s) trouvé(es)");
        } catch (ValueDataException e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        return list;
    }

    @Override
    public Boolean avisVerification(String numeroDeclaration, String avis, String observation, Integer userId) {
        Boolean u = false;

        try {

            AppUtilities.controlValue(numeroDeclaration, "Veuillez renseigner la declaration");
            AppUtilities.controlValue(avis, "Veuillez renseigner votre avis");
            AppUtilities.controlValue(observation, "Veuillez renseigner votre observation");

            DaoDeclaration dao = new DaoDeclaration();
            Declaration d = (Declaration) dao.findByNumero(Declaration.class, numeroDeclaration);
            if (d == null) {
                throw new ValueDataException("Cette declaration n'existe pas");
            }
            d.setAvis(avis);
            d.setObservationPrepose(observation);
            d.setFkPrepose(userId);
            u = dao.saveOrUpdate(d);
            if (u == null || !u) {
                throw new ValueDataException("l'opération n'a pas abouti");
            }
            this.error.setErrorCode(ErrorResponse.OK);
        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        return u;
    }

    @Override
    public Boolean avisAuthentification(String numeroDeclaration, String avis, String observation, Integer userId) {
        Boolean u = null;

        try {
            if (numeroDeclaration == null || numeroDeclaration == "") {
                throw new ValueDataException("Veuillez Renseigner un identifiant valide ");
            }
            DaoDeclaration dao = new DaoDeclaration();
            Declaration d = (Declaration) dao.findByNumero(Declaration.class, numeroDeclaration);
            if (d == null) {
                throw new ValueDataException("Cette declaration n'existe pas");
            }
            d.setAvis(avis);
            d.setObservationOec(observation);
            d.setFkOec(userId);

            if (!dao.saveOrUpdate(d)) {
                throw new ValueDataException("l'opération n'a pas abouti");
            }
            this.error.setErrorCode(ErrorResponse.OK);
        } catch (ValueDataException e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        return u;
    }

    @Override
    public String createAttestation(Object obj, Integer userId, String type) {
        String numero = null;
        error = new ErrorResponse();

        try {

            AppUtilities.controlValue(type, "Veuillez renseigner le type");

            switch (type) {
                case SaveaAttestationApi.ATTESTATION_NAISSANCE:
                    numero = saveAttestationNaissance((AttestationNaissance) obj, userId);
                    break;
                default:
                    throw new ValueDataException("DOCUMENT NO-PRISE EN CHARGE");
            }

            AppUtilities.controlValue(numero, error.getErrorDescription());

            error.setErrorCode("OK");
            error.setErrorDescription("Attestation créée avec Le numéro: " + numero);
        } catch (ValueDataException e) {
            error.setErrorCode("KO");
            error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            error.setErrorCode("KO");
            error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return numero;
    }

    @Override
    public String createProcuration(Object procuration, Integer userId, String type) {
        String numero = null;
        error = new ErrorResponse();

        try {
            AppUtilities.controlValue(userId, "Veuillez renseigner l'agent");
            AppUtilities.controlValue(type, "Veuillez selectionner le type de procuration");

            switch (type) {
                case SaveProcurationApi.PROCURATION_NAISSANCE:
                    numero = saveProcurationNaissance((ProcurationNaissance) procuration, userId);
                    break;
                case SaveProcurationApi.PROCURATION_DECES:
                    numero = saveProcurationDeces((ProcurationDeces) procuration, userId);
                    break;

                default:
                    throw new ValueDataException("Document non-prise en charge");
            }

            AppUtilities.controlValue(numero, error.getErrorDescription());

            error.setErrorCode(ErrorResponse.OK);
            error.setErrorDescription("Procuration créée avec succès");

        } catch (ValueDataException e) {
            error.setErrorCode(ErrorResponse.KO);
            error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            error.setErrorCode(ErrorResponse.KO);
            error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return numero;
    }

    private String saveProcurationNaissance(ProcurationNaissance procuration, Integer userId) {
        String numero = null;
        error = new ErrorResponse();

        try {

            AppUtilities.controlValue(procuration.getFkEnfant(), "Veuillez identifier l'enfant");
            AppUtilities.controlValue(procuration.getFkMere(), "Veuillez identifier la mère");
            AppUtilities.controlValue(procuration.getFkPere(), "Veuillez identifier le père");
            AppUtilities.controlValue(procuration.getInteresse(), "Veuillez identifier l'interessé");
            AppUtilities.controlValue(procuration.getMandataire(), "Veuillez renseigner le mandataire");
            AppUtilities.controlValue(procuration.getPersonneContact(), "Veuillez identifier la personne à contacter");
            AppUtilities.controlValue(procuration.getSituationMatrimoniale(), "Veuillez reseigner la situation matrimoniale");
            AppUtilities.controlValue(procuration.getPreuvePaiement(), "Veuillez renseigner le mandataire");

            if (verifierPreuvePaiement(procuration.getPreuvePaiement()) == null) {
                throw new ValueDataException(error.getErrorDescription());
            }

            procuration.setAgentCreat(userId);
            procuration.setDateCreat(new Date());
            procuration.setStatut(true);

            DaoProcuration dao = new DaoProcuration();

            numero = dao.saveProcurationNaissance(procuration);

            AppUtilities.controlValue(numero, "L'enregistrement de la procuration de naissance a échoué");

            error.setErrorCode("OK");

        } catch (ValueDataException e) {
            error.setErrorCode("KO");
            error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            error.setErrorCode("KO");
            error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return numero;
    }

    private String saveProcurationDeces(ProcurationDeces procuration, Integer userId) {
        String numero = null;
        error = new ErrorResponse();

        try {

            AppUtilities.controlValue(procuration.getFkDefunt(), "Veuillez identifier le defunt");
            AppUtilities.controlValue(procuration.getFkMere(), "Veuillez identifier la mère");
            AppUtilities.controlValue(procuration.getFkPere(), "Veuillez identifier le père");
            AppUtilities.controlValue(procuration.getInteresse(), "Veuillez identifier l'interessé");
            AppUtilities.controlValue(procuration.getMandataire(), "Veuillez renseigner le mandataire");
            AppUtilities.controlValue(procuration.getPreuvePaiement(), "Veuillez renseigner le mandataire");

            if (verifierPreuvePaiement(procuration.getPreuvePaiement()) == null) {
                throw new ValueDataException(error.getErrorDescription());
            }

            procuration.setAgentCreat(userId);
            procuration.setDateCreat(new Date());
            procuration.setStatut(true);

            DaoProcuration dao = new DaoProcuration();

            numero = dao.saveProcurationDeces(procuration);

            AppUtilities.controlValue(numero, "L'enregistrement de la procuration de décès a échoué");

            error.setErrorCode("OK");

        } catch (ValueDataException e) {
            error.setErrorCode("KO");
            error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            error.setErrorCode("KO");
            error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return numero;
    }

    private String saveAttestationNaissance(AttestationNaissance attestation, Integer userId) {
        String numero = null;
        error = new ErrorResponse();

        try {

            if (attestation == null) {
                throw new ValueDataException("Veuillez remplir correctement l'attestation");
            }

            AppUtilities.controlValue(userId, "Veuillez renseigner l'agent");
            AppUtilities.controlValue(attestation.getFkEnfant(), "Veuillez renseigner l'enfant");
            AppUtilities.controlValue(attestation.getFkMere(), "Veuillez renseigner la mère");
            AppUtilities.controlValue(attestation.getFkPere(), "Veuillez renseigner le père");
            AppUtilities.controlValue(attestation.getLieuNaissance(), "Veuillez renseigner le lieu de naissance");
            AppUtilities.controlValue(attestation.getPreuvePaiement(), "Veuillez renseigner la preuve de paiement");

            if (verifierPreuvePaiement(attestation.getPreuvePaiement()) == null) {
                throw new ValueDataException(error.getErrorDescription());
            }

            attestation.setAgentCreat(userId);
            if (attestation.getDateNaissance() == null) {
                throw new ValueDataException("Veuillez renseigner la date de naissance");
            }

            DaoAttestation dao = new DaoAttestation();

            numero = dao.saveAttestationNaissance(attestation);

            AppUtilities.controlValue(numero, "Attestation de naissance n'a pas été créée");

            error.setErrorCode("OK");
        } catch (ValueDataException e) {
            error.setErrorCode(ErrorResponse.KO);
            error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            error.setErrorCode("KO");
            error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return numero;
    }
}
