/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import contracts.IAuthentification;
import daos.DaoAuthentification;
import entities.ErrorResponse;
import entities.User;
import entities.ValueDataException;
import entities.utilities.UserAdapter;
import others.AppUtilities;
import utilities.AppConst;

/**
 *
 * @author Trinix
 */
public class AuthentificationController implements IAuthentification{
    
    private ErrorResponse error;

    public AuthentificationController() {
        this.error = new ErrorResponse();
    }

    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }
    
    

    @Override
    public UserAdapter connexion(String username, String password) {
        UserAdapter user = null;
        
        try{
            AppUtilities.controlValue(username, "Veuillez renseigner le nom d'utilisateur");
            AppUtilities.controlValue(password, "Veuillez renseigner le mot de passe");
            
            DaoAuthentification dao = new DaoAuthentification();
            
            user = dao.connexion(username, password);
            
            if(user == null)
                throw new ValueDataException("Nom d'utilisateur ou mot de passe incorrect");
            
            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("Utilisateur authentifié");
            
        }catch(ValueDataException e){
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return user;
    }

    @Override
    public User getUser(String username, String token) {
        error = new ErrorResponse();
        User user = null;
        
        try{
            AppUtilities.controlValue(username, "Renseignez l'utilisateur");
            AppUtilities.controlValue(token, "Renseignez le token");
            
            user = new DaoAuthentification().getUser(username, token);
            if(user==null)
                throw new ValueDataException("Votre session a expiré");
            
            error.setErrorCode(ErrorResponse.OK);
            error.setErrorDescription("Token correct");
            
        }catch(ValueDataException e){
            error.setErrorCode(ErrorResponse.KO);
            error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            error.setErrorCode(ErrorResponse.KO);
            error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return user;
    }
    
}
