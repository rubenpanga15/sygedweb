/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import contracts.IIdentificationController;
import daos.DaoIdentification;
import entities.ErrorResponse;
import entities.Personne;
import entities.ValueDataException;
import java.util.List;
import others.AppUtilities;
import utilities.AppConst;

/**
 *
 * @author Trinix
 */
public class IdentificationController implements IIdentificationController {
    
    private ErrorResponse error;

    public IdentificationController() {
        this.error = new ErrorResponse();
    }

    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }
    
    
    
    @Override
    public Integer savePersonne(Personne personne) {
        Integer id = null;
        
        try{
            AppUtilities.controlValue(personne.getNom(), "Vruillez inserer le nom");
            AppUtilities.controlValue(personne.getPostNom(), "Veuuillez renseigner le postnom");
            AppUtilities.controlValue(personne.getPrenom(), "Veuillez renseigner le prenom");
            AppUtilities.controlValue(personne.getLieuNaissance(), "Veuillez renseigner le nom de naissance");
            AppUtilities.controlValue(personne.getSexe(), "Veuillez renseigner le sexe");
            
            DaoIdentification dao = new DaoIdentification();
            
            if(personne.getId() == null)
                id = dao.save(personne);
            else{
                if(dao.saveOrUpdate(personne))
                    id = personne.getId();
            }
            
            
            if(id == null)
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            
            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("Opération effectuée avec succès");
                
        }catch(ValueDataException e){
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW); 
        }
        
        return id;
    }

    @Override
    public List<Personne> searchPersonne(String motclef) {
        List<Personne> list = null;
        
        try{
            AppUtilities.controlValue(motclef, "Veuillez saisir le mot clé");
            
            DaoIdentification dao = new DaoIdentification();
            
            list = dao.getListPersonne(motclef);
            
            if(list == null || list.size() <= 0)
                throw new ValueDataException(AppConst.EMPTY_LIST);
            
            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " Personne retrouvées");
        }catch(ValueDataException e){
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return list;
    }
    
}
