/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import contracts.IConfigurationController;
import daos.DaoConfiguration;
import entities.Config;
import entities.Documentprocedure;
import entities.ErrorResponse;
import entities.Procedure;
import entities.ValueDataException;
import java.util.List;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author Trinix
 */
public class ConfigurationController implements IConfigurationController {
    private ErrorResponse error;

    @Override
    public ErrorResponse getError() {
        return error;
    }
    
    
    @Override
    public List<Documentprocedure> getDocumentNecaissaire(Integer fkProcedure){
        List<Documentprocedure> list = null;
        error = new ErrorResponse();
        
        try{
            AppUtilities.controlValue(fkProcedure, "Veuillez renseigner le procedure");
            
            DaoConfiguration dao = new DaoConfiguration();
            
            list = dao.getDocuments(fkProcedure);
            
            if(list == null || list.size() <= 0)
                throw new ValueDataException(AppConst.EMPTY_LIST);
            
        }catch(ValueDataException e){
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return list;
    }

    @Override
    public Config getConfig(String key) {
        Config obj = null;
        error = new ErrorResponse();
        
        try{
            AppUtilities.controlValue(key, "Renseignez le code du système");
            
            obj = (Config) new DaoConfiguration().findByCode(Config.class, key);
            
            if(obj==null)
                throw new ValueDataException("Système demandé non configurée");
            
            error.setErrorCode(ErrorResponse.OK);
            error.setErrorDescription("Configuration du système trouvé");
            
        }catch(ValueDataException e){
            error.equals(ErrorResponse.KO);
            error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            error.equals(ErrorResponse.KO);
            error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return obj;
    }
    
    public List<Procedure> getListProcedures(){
        List<Procedure> list = null;
        
        this.error = new ErrorResponse();
        
        try{
            
            DaoConfiguration dao = new DaoConfiguration();
            
            list = dao.getProcedures();
            
            if(list == null || list.size() <= 0)
                throw new ValueDataException("Aucune procédure n'a été configurée");
            
            this.error.setErrorCode("OK");
            this.error.setErrorDescription(list.size() + " procedure(s) trouvées");
            Logger.printLog("CTRL", "ICo");
            
        }catch(ValueDataException e){
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return list;
    }
}
