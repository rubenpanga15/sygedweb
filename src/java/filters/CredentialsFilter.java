/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filters;

import controllers.AuthentificationController;
import entities.ErrorResponse;
import entities.User;
import entities.ValueDataException;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author christianeboma
 */
@WebFilter(asyncSupported = true, urlPatterns = {"/api/*"})
public class CredentialsFilter implements Filter {

    private static final String TAG = CredentialsFilter.class.getSimpleName();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //Filter.super.init(filterConfig); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest requestServlet = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        HttpDataResponse httpDataResponse = new HttpDataResponse();
        User user = null;
        String requestBody = null;
        
        try {
            requestBody = HttpUtilities.getBodyRequest(requestServlet);
            AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);
            Logger.printLog(TAG, requestBody);

            String username = null, token = null, numero = null;
            try {
                JSONObject json = new JSONObject(requestBody);
                
                if (json.has("username")) {
                    username = json.getString("username");
                }

                if (json.has("token")) {
                    token = json.getString("token");
                }

            } catch (Exception e) {
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }
            
            AuthentificationController controller = new AuthentificationController();
            
            user = controller.getUser(username, token);
            
            httpDataResponse.setError(controller.getError());

        } catch (ValueDataException e) {
            httpDataResponse.getError().setErrorCode(ErrorResponse.KO);
            httpDataResponse.getError().setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();

            httpDataResponse.getError().setErrorCode(ErrorResponse.KO);
            httpDataResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
        } finally {
            if (httpDataResponse.getError().getErrorCode().equals(ErrorResponse.OK)) {
                request.setAttribute("user", user);
                request.setAttribute("query", requestBody);
                chain.doFilter(request, response);
            } else {
                
                
                HttpUtilities.prepareHttpResponse(httpResponse, httpDataResponse).flush();
            }
        }
    }

    @Override
    public void destroy() {
        //Filter.super.destroy(); //To change body of generated methods, choose Tools | Templates.
    }

}
