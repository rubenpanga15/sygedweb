/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author ghost06
 */
public class AppConst {
    
    public static final String ERROR_UNKNOW = "SyGED - Erreur inconnue survenue, contatez le support technique.";
    public static final String ERROR_SERVLET_UNKNOW = "SyGED - Erreur inconnue au moment du traitement.";
    public static final String NO_PERMISSION = "SyGED - Vous n'avez pas droit à effecuter cette action.";
    public static final String PARMS_EMPTY = "SyGED - Aucun paramètre reçu.";
    public static final String PARAMS_INCORRECT_FORMAT ="SyGED - Paramètres incorrectes.";
    public static final String AGENT_DOESNT_EXIST = "SyGED - Votre compte utilisateur n'est relié à aucun agent.";
    public static final String OPERATION_FAILED = "SyGED - L'opération n'a pas abouti.";
    public static final String EMPTY_LIST = "SyGED - Aucune donnée retrouvée.";
    
}
