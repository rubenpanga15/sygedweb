/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contracts;

import entities.Attestation;
import entities.Declaration;
import entities.Document;
import entities.Documentprocedure;
import entities.Lienarchive;
import entities.Procuration;
import entities.utilities.AttestationRequest;
import entities.utilities.DeclarationAdapter;
import entities.utilities.DocumentRequest;
import entities.utilities.ProcurationRequest;
import entities.utilities.UploadFileForDeclarationRequestItem;
import entities.utilities.UploadFileForDeclarationResponse;
import entities.utilities.UplodeFileForDeclarationRequest;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AdolphTrinix
 */
public interface IDeclaration extends IController {
    
    public Declaration createDeclation(String centre, Integer fkBeneficiaire, Integer fkRequerant, String langue, Integer fkInterprete, Integer fkRedacteur, String observationRedacteur,String type, String refPaiement, int userId, Object acte, List<DocumentRequest> documents);
    public List<Declaration>getAllDeclaration();
    public Declaration getDeclaration(String numero);
    public Boolean deleteDeclaration(String numero);
    public Declaration updateDeclaration(String numero,Integer fkBeneficiaire, Integer fkRequerant, String langue, Integer fkInterprete, Integer fkRedacteur, String observationRedacteur,Integer fkPrepose, String observationPrepose, Integer fkOec, String observationOec, String avis, String type);
    public List<Documentprocedure> getDocumentAArchiver(String typeActe);
    public List<Lienarchive> saveDocumentLink(List<String> base64FileContent, String numeroDeclaration, Integer typeDocument, Integer userId);
    public List<UploadFileForDeclarationResponse> saveDocumentLink(List<UploadFileForDeclarationRequestItem> items, int userId);
    public List<Declaration> getDeclarationByPeriode(String debut, String fin, String centre);
    Boolean avisVerification(String numeroDeclaration, String avis, String observation, Integer userId);
    Boolean avisAuthentification(String numeroDeclaration, String avis, String observation, Integer userId);
    public String createAttestation(Object obj, Integer userId, String type);
    public String createProcuration(Object procuration, Integer userId, String type);
    
    
}
