/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contracts;

import entities.Personne;
import java.util.List;

/**
 *
 * @author Trinix
 */
public interface IIdentificationController {
    public Integer savePersonne(Personne personne);
    public List<Personne> searchPersonne(String motclef);
}
