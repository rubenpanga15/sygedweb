/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contracts;

import entities.Config;
import entities.Documentprocedure;
import java.util.List;

/**
 *
 * @author christianeboma
 */
public interface IConfigurationController extends IController {
    List<Documentprocedure> getDocumentNecaissaire(Integer fkProcedure);
    Config getConfig(String key);
}
