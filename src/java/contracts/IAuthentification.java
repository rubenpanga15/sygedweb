/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contracts;

import entities.User;
import entities.utilities.UserAdapter;

/**
 *
 * @author Trinix
 */
public interface IAuthentification {
    public UserAdapter connexion(String username, String password);
    public User getUser(String username, String token);
}
