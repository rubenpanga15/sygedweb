/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import entities.Personne;
import entities.QueryParam;
import entities.User;
import entities.utilities.UserAdapter;
import java.util.List;

/**
 *
 * @author Trinix
 */
public class DaoAuthentification extends DaoGeneric {

    public UserAdapter connexion(String username, String password) {
        UserAdapter user = null;

        String sql
                = "SELECT\n"
                + "`user`.id id0,\n"
                + "`user`.username,\n"
                + "`user`.`password`,\n"
                + "`user`.matricule,\n"
                + "`user`.role,\n"
                + "`user`.token,\n"
                + "personne.id id1,\n"
                + "personne.nom,\n"
                + "personne.postNom,\n"
                + "personne.prenom,\n"
                + "personne.lieuNaissance,\n"
                + "personne.dateNaissance,\n"
                + "personne.sexe,\n"
                + "personne.telephone,\n"
                + "personne.email,\n"
                + "personne.profession,\n"
                + "personne.etatCivil,\n"
                + "personne.nomPere,\n"
                + "personne.nomMere,\n"
                + "personne.province,\n"
                + "personne.district,\n"
                + "personne.secteur,\n"
                + "personne.groupement,\n"
                + "personne.adresse,\n"
                + "`user`.fkCentre centre\n"
                + "FROM\n"
                + "`user` AS `user`\n"
                + "Inner Join personne AS personne\n"
                + "WHERE\n"
                + "`user`.username =  :username AND\n"
                + "`user`.`password` =  :password";
        
        List<Object[]> list = this.selectSQL(sql, new QueryParam("username", username), new QueryParam("password", password));
        
        if(list != null && list.size() > 0){
            user = new UserAdapter();
            
            for(Object[] row: list){
                try{
                    user.setId((Integer) row[0]);
                }catch(Exception e){e.printStackTrace();}
                
                try{
                    user.setUsername((String) row[1]);
                }catch(Exception e){e.printStackTrace();}
                
                try{
                    user.setPassword((String) row[2]);
                }catch(Exception e){e.printStackTrace();}
                
                try{
                    user.setRole((String) row[4]);
                }catch(Exception e){e.printStackTrace();}
                
                try{
                    user.setToken((String) row[5]);
                }catch(Exception e){e.printStackTrace();}
                user.setPersonne(new Personne());
                try{
                    user.getPersonne().setNom((String) row[7]);
                }catch(Exception e){e.printStackTrace();}
                
                try{
                    user.getPersonne().setPostNom((String) row[8]);
                }catch(Exception e){e.printStackTrace();}
                
                try{
                    user.getPersonne().setPrenom((String) row[9]);
                }catch(Exception e){e.printStackTrace();}
                
                try{
                    user.setFkCentre((String) row[24]);
                    
                }catch(Exception e){e.printStackTrace();}
            }
        }

        return user;
    }

    
    public User getUser(String username, String token){
        User obj = null;
        String hql = "from User where username=:username and token=:token";
        try{
            obj = (User) this.selectHQL(hql, new QueryParam("username", username), new QueryParam("token", token)).get(0);
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return obj;
    }
}
