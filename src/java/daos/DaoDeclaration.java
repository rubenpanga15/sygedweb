/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import entities.Actedeces;
import entities.Actemariage;
import entities.Actenaissance;
import entities.Attestation;
import entities.Declaration;
import entities.Document;
import entities.Documentprocedure;
import entities.Lienarchive;
import entities.Personne;
import entities.QueryParam;
import entities.Typedocument;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import others.Logger;

/**
 *
 * @author Trinix
 */
public class DaoDeclaration extends DaoGeneric {

    public boolean pushPreuvePaiement(String numeroDeclaration, String numRef){
        boolean success = false;
        try{
            String sql = "update declaration set numeroRef=:numRef where numero=:numero";
            success = this.executeSQLQuery(sql, new QueryParam("numRef", numRef), new QueryParam("numero", numeroDeclaration));
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return success;
    }
    
    public boolean quittanceIsLocallyUsed(String numero){
        boolean success = false;
        try{
            String hql = "from Declaration where numeroRef=:numero and statut=1";
            this.selectHQL(hql, new QueryParam("numero", numero)).get(0);
            success = true;
            
        }catch(Exception e){
            e.printStackTrace();
        }
        return success;
    }
    public Document getDocument(String numeroDeclaration, int typeDocumentId){
        Document obj = null;
        
        try{
            String hql = "from Document where fkDeclaration=:declaration and fkTypeDocument=:typeDocument";
            obj = (Document) this.selectHQL(hql, new QueryParam("declaration", numeroDeclaration), new QueryParam("typeDocument", typeDocumentId)).get(0);
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return obj;
    }
    
    public List<Document> getDocuments(String numeroDeclaration){
        List<Document> list = null;
        
        String hql = "from Document where fkDeclaration =:fk";
        
        list = this.selectHQL(hql, new QueryParam("fk", numeroDeclaration));
        
        List<Document> documents = new ArrayList<>();
        
        for(Document document: list){
            document.setLienArchives(getLienArchiveForDocument(document.getNumero()));
            
            document.setTypeDocument((Typedocument) this.findById(Typedocument.class, document.getFkTypeDocument()));
            
            documents.add(document);
        }
        
        return documents;
    }
    
    public List<Lienarchive> getLienArchiveForDocument(String fkDocument){
        List<Lienarchive> list = null;
        
        try{
            String hql = "from Lienarchive where fkDocument=:document";
            list = this.selectHQL(hql, new QueryParam("document", fkDocument));
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return list;
    }
    
    public List<Documentprocedure> getDocumentAArchiver(String typeActe) {
        List<Documentprocedure> list = null;

        try {
            String sql = "select dp.id as dpid, dp.fkTypedocument, dp.isObligatoire, dp.fkProcedure, dp.statut, dp.dateCreat, td.id as tdid, td.description as tddescription, td.aScanner from documentProcedure dp inner join typeDocument td on dp.fkTypeDocument=td.id inner join syged.procedure p on dp.fkProcedure=p.id where p.type=:type and dp.statut=1";
            List<Object[]> dataSet = this.selectSQL(sql, new QueryParam("type", typeActe));

            if (dataSet != null && dataSet.size() > 0) {
                for (Object[] row : dataSet) {
                    Documentprocedure obj = new Documentprocedure();

                    try {
                        obj.setId((Integer) row[0]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    
                    try {
                        obj.setFkTypeDocument((Integer) row[1]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    
                    try {
                        obj.setIsObligatoire((Boolean) row[2]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    
                    try {
                        obj.setFkProcedure((Integer) row[3]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    
                    try {
                        obj.setStatut((Boolean) row[4]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    
                    try {
                        obj.setDateCreat((Date) row[5]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    
                    try {
                        obj.setTypeDocument(new Typedocument());
                        obj.getTypeDocument().setId((Integer) row[6]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    
                    try {
                        obj.getTypeDocument().setDescription((String) row[7]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    
                    try {
                        obj.getTypeDocument().setAscanner((Boolean) row[8]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (list == null) {
                        list = new ArrayList();
                    }

                    list.add(obj);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }
    
    public String saveDeclaration(Declaration obj){
//        String sql = "insert declaration set numero='', fkBeneficiaire=:fkBeneficiaire, "
//                + "fkRequerant=:fkRequerant, langue=:langue, "
//                + "fkInterprete=:fkInterprete, fkRedacteur=:fkRedacteur, "
//                + "observationRedacteur=:observationRedacteur, "
//                + "fkCentre=:fkCentre, dateCreat=now(), statut=1";
String sql = "insert declaration(fkBeneficiaire, fkRequerant, langue, fkInterprete, fkRedacteur, observationRedacteur, fkCentre, dateCreat, statut, type) values (:fkBeneficiaire, "
                + ":fkRequerant,:langue, "
                + ":fkInterprete, :fkRedacteur, "
                + ":observationRedacteur, "
                + ":fkCentre, now(), statut=1, :type)";
        
        try{
            if(this.executeSQLQuery(sql, 
                    //new QueryParam("numero", obj.getNumero()),
                    new QueryParam("fkBeneficiaire", obj.getFkBeneficiaire()),
                    new QueryParam("fkRequerant", obj.getFkRequerant()),
                    new QueryParam("langue", obj.getLangue()),
                    new QueryParam("fkInterprete", obj.getFkInterprete()),
                    new QueryParam("fkRedacteur", obj.getFkRedacteur()),
                    new QueryParam("observationRedacteur", ""),
                    new QueryParam("fkCentre", obj.getFkCentre()),
                    new QueryParam("type", obj.getType())
                    )){
                
                sql = "select numero from declaration where fkRedacteur=:agentCreat order by dateCreat desc limit 1";
                obj.setNumero((String) this.selectSQL(sql, new QueryParam("agentCreat", obj.getFkRedacteur())).get(0));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return obj.getNumero();
    }
    
    public String saveActeNaissance(Actenaissance obj){
        String sql = "insert acteNaissance set numero=:numero, lieuNaissance=:lieuNaissance, dateNaissance=:dateNaissance, fkPere=:fkPere, fkMere=:fkMere, fkDeclaration=:fkDeclaration, statut=true, dateCreat=now(), fkCentre=:fkCentre, agentCreat=:agentCreat";
        
        try{
            if(this.executeSQLQuery(sql, new QueryParam("numero", obj.getNumero()), 
                    new QueryParam("lieuNaissance", obj.getLieuNaissance()), 
                    new QueryParam("dateNaissance", obj.getDateNaissance()), 
                    new QueryParam("fkPere", obj.getFkPere()), 
                    new QueryParam("fkMere", obj.getFkMere()),
                    new QueryParam("fkDeclaration", obj.getFkDeclaration()),
                    new QueryParam("fkCentre", obj.getFkCentre()),
                    new QueryParam("agentCreat", obj.getAgentCreat())
            )){
                sql = "select numero from acteNaissance where agentCreat=:agentCreat order by dateCreat desc limit 1";
                obj.setNumero((String) this.selectSQL(sql, new QueryParam("agentCreat", obj.getAgentCreat())).get(0));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return obj.getNumero();
    }
    
    public String saveActeDeces(Actedeces obj){
        String sql = "insert acteDeces set numero='', etatCivil=:etatCivil, circonstance=:circonstance, lieuDeces=:lieuDeces, dateDeces=:dateDeces, fkPere=:fkPere, fkMere=:fkMere, fkDeclaration=:fkDeclaration, statut=true, dateCreat=now(), fkCentre=:fkCentre, agentCreat=:agentCreat";
        
        try{
            if(this.executeSQLQuery(sql, 
                    new QueryParam("lieuDeces", obj.getLieuDeces()), 
                    new QueryParam("dateDeces", obj.getDateDeces()), 
                    new QueryParam("fkPere", obj.getFkPere()), 
                    new QueryParam("fkMere", obj.getFkMere()),
                    new QueryParam("fkDeclaration", obj.getFkDeclaration()),
                    new QueryParam("fkCentre", obj.getFkCentre()),
                    new QueryParam("circonstance", obj.getCirconstance()),
                    new QueryParam("etatCivil", obj.getEtatCivil()),
                    new QueryParam("agentCreat", obj.getAgentCreat())
            )){
                sql = "select numero from acteDeces where agentCreat=:agentCreat order by dateCreat desc limit 1";
                obj.setNumero((String) this.selectSQL(sql, new QueryParam("agentCreat", obj.getAgentCreat())).get(0));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return obj.getNumero();
    }
    
    public String saveActeMariage(Actemariage obj){
        String sql = "insert acteMariage set numero='', regimeMatrimonial=:regimeMatrimonial, lieuPublicite=:lieuPublicite, dateMariage=:dateMariage, fkPereEpoux=:fkPereEpoux, fkMereEpoux=:fkMereEpoux, fkPereEpouse=:fkPereEpouse, fkMereEpouse=:fkMereEpouse, fkDeclaration=:fkDeclaration, statut=true, dateCreat=now(), fkCentre=:fkCentre, agentCreat=:agentCreat";
        
        try{
            if(this.executeSQLQuery(sql, 
                    new QueryParam("lieuPublicite", obj.getLieuPublicite()), 
                    new QueryParam("dateMariage", obj.getDateMariage()), 
                    new QueryParam("fkPereEpoux", obj.getFkPereEpoux()), 
                    new QueryParam("fkMereEpoux", obj.getFkMereEpoux()),
                    new QueryParam("fkPereEpouse", obj.getFkPereEpouse()), 
                    new QueryParam("fkMereEpouse", obj.getFkMereEpouse()),
                    new QueryParam("fkDeclaration", obj.getFkDeclaration()),
                    new QueryParam("fkCentre", obj.getFkCentre()),
                    new QueryParam("regimeMatrimonial", obj.getRegimeMatrimonial()),
                    new QueryParam("agentCreat",obj.getAgentCreat())
            )){
                sql = "select numero from acteMariage where agentCreat=:agentCreat order by dateCreat desc limit 1";
                obj.setNumero((String) this.selectSQL(sql, new QueryParam("agentCreat", obj.getAgentCreat())).get(0));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return obj.getNumero();
    }
    
    public Declaration getDeclaration(String numero){
        Declaration declaration = (Declaration) this.findByNumero(Declaration.class, numero);
        
        if(declaration != null){
            DaoIdentification dao = new DaoIdentification();
            declaration.setBeneficiaire(dao.getPersonn(declaration.getFkBeneficiaire()));
            declaration.setRequerant(dao.getPersonn(declaration.getFkRequerant()));
            declaration.setInterprete(dao.getPersonn(declaration.getFkInterprete()));
            declaration.setRedacteur(dao.getUser(declaration.getFkRedacteur()));
            declaration.setPrepose(dao.getUser(declaration.getFkPrepose()));
            declaration.setOec(dao.getUser(declaration.getFkOec()));
            
            switch(declaration.getType()){
                case Declaration.TYPE_ACTE_NAISSANCE:
                    Logger.printLog("CONTROLE", "ACTE DE NAISSANCE");
                    declaration.setActeNaissance((Actenaissance) getActe(Declaration.TYPE_ACTE_NAISSANCE, declaration.getNumero()));
                    break;
                case Declaration.TYPE_ACTE_MARIAGE:
                    declaration.setActeMariage((Actemariage) getActe(Declaration.TYPE_ACTE_MARIAGE, declaration.getNumero()));
                    break;
                case Declaration.TYPE_ACTE_DECES:
                    declaration.setActeDeces((Actedeces) getActe(Declaration.TYPE_ACTE_DECES, declaration.getNumero()));
                    break;
            }
            
            declaration.setDocuments(getDocuments(declaration.getNumero()));
            
        }
        
        return declaration;
    }
    
    public Object getActe(String type, String fkDeclaration){
        Object obj = null;
        
        switch(type){
            case Declaration.TYPE_ACTE_NAISSANCE:
                obj = getActeNaissance(fkDeclaration);
                break;
            case Declaration.TYPE_ACTE_MARIAGE:
                obj = getActeMariage(fkDeclaration);
                break;
            case Declaration.TYPE_ACTE_DECES:
                obj = getActeDeces(fkDeclaration);
                break;
                
        }
        
        return obj;
    }
    
    private Actenaissance getActeNaissance(String fkDeclaration){
        Actenaissance acte = null;
        
        String hql = "from Actenaissance where fkDeclaration =:fk";
        
        List<Actenaissance> list = this.selectHQL(hql, new QueryParam("fk", fkDeclaration));
        
        
        if(list != null && list.size() > 0)
            acte = list.get(0);
        
        if(acte != null){
            try{
                acte.setPere((Personne) this.findById(Personne.class, acte.getFkPere()));
                acte.setMere((Personne) this.findById(Personne.class, acte.getFkMere()));
            }catch(Exception e){}
        }
        
        
        return acte;
    }
    
    private Actedeces getActeDeces(String fkDeclaration){
        Actedeces acte = null;
        
        String hql = "from Actedeces where fkDeclaration =:fk";
        
        List<Actedeces> list = this.selectHQL(hql, new QueryParam("fk", fkDeclaration));
        
        
        if(list != null && list.size() > 0)
            acte = list.get(0);
        
        return acte;
    }
    
    private Actemariage getActeMariage(String fkDeclaration){
        Actemariage acte = null;
        
        String hql = "from Actemariage where fkDeclaration =:fk";
        
        List<Actemariage> list = this.selectHQL(hql, new QueryParam("fk", fkDeclaration));
        
        
        if(list != null && list.size() > 0)
            acte = list.get(0);
        
        return acte;
    }

    public List<Declaration> getAllDeclarat() {
        List<Declaration> list = null;
        try {
            String hql = "from Declaration where statut !=0";
        
        list = this.selectHQL(hql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        
        return list;
    }

    public String saveAttestation(Attestation obj) {
        String sql = "insert attestation set numero=:numero , fkPere=:fkPere, "
                + "fkMere=:fkMere, lieuNaissance=:lieuNaissance, "
                + "agentCreat=:agentCreat, fkCentre=:fkCentre, "
                + "dateNaissance=:dateNaissance, "
                + "dateCreat=now(), statut=1";
        
        try{
            if(this.executeSQLQuery(sql, 
                    new QueryParam("numero", obj.getNumero()),
                    new QueryParam("fkRedacteur", obj.getFkPere()),
                    new QueryParam("fkBeneficiaire", obj.getFkMere()),
                    new QueryParam("langue", obj.getLieuNaissance()),
                    new QueryParam("fkInterprete", obj.getFkCentre()),
                    new QueryParam("observationRedacteur", obj.getDateNaissance())
                    )){
                
                sql = "select numero from attestation where agentCreat=:agentCreat order by dateCreat desc limit 1";
                obj.setNumero((String) this.selectSQL(sql, new QueryParam("agentCreat", obj.getAgentCreat())).get(0));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return obj.getNumero();
    
    }
    
    public List<Declaration> getDeclaration(Date dateDebut, Date dateFin){
        List<Declaration> list = null;
        
        List<Declaration> list2 = new ArrayList<>();
        
        String hql = "from Declaration where dateCreat between :debut and :fin";
        
        list = this.selectHQL(hql, new QueryParam("debut", dateDebut), new QueryParam("fin", dateFin));
        
        if(list != null){
            for(Declaration declaration: list){
                try{
                    declaration = this.getDeclaration(declaration.getNumero());
                }catch(Exception e){e.printStackTrace();}
                
                list2.add(declaration);
            }
        }
        
        return list2;
    }
    
    public List<Declaration> getDeclaration(Date dateDebut, Date dateFin, String type, String critere){
        List<Declaration> dataset = null;
        List<Declaration> data = new ArrayList<>();
        
        
        
        return data;
    }
}
