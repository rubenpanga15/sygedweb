/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import entities.ProcurationDeces;
import entities.ProcurationNaissance;
import entities.QueryParam;

/**
 *
 * @author AdolphTrinix
 */
public class DaoProcuration extends DaoGeneric {
    
    public String saveProcurationNaissance(ProcurationNaissance obj){
        String sql =
                "insert procurationNaissance set numero = '', "
                + " interesse =:interesse, fkPere =:fkPere, fkMere =:fkMere, fkEnfant =:fkEnfant,"
                + " situationMatrimoniale =:situation, divorceLe =:divorceLe, mandataire =:mandataire, personneContact=:personneContact, "
                + " agentCreat =:agent, dateCreat = now(), statut = 1, fkCentre=:fkCentre, preuvePaiement=:preuvePaiement";
        
        try{
            if(this.executeSQLQuery(sql, 
                    new QueryParam("interesse", obj.getInteresse()),
                    new QueryParam("fkPere", obj.getFkPere()),
                    new QueryParam("fkMere", obj.getFkMere()),
                    new QueryParam("fkEnfant", obj.getFkEnfant()),
                    new QueryParam("situation", obj.getSituationMatrimoniale()),
                    new QueryParam("divorceLe", obj.getDivorceLe()),
                    new QueryParam("mandataire", obj.getMandataire()),
                    new QueryParam("agent", obj.getAgentCreat()),
                    new QueryParam("fkCentre", obj.getFkCentre()),
                    new QueryParam("preuvePaiement", obj.getPreuvePaiement()),
                    new QueryParam("personneContact", obj.getPersonneContact())
                    )){
                
                sql = "select numero from procurationNaissance where agentCreat=:agentCreat order by dateCreat desc limit 1";
                obj.setNumero((String) this.selectSQL(sql, new QueryParam("agentCreat", obj.getAgentCreat())).get(0));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return obj.getNumero();
    }
    
    public String saveProcurationDeces(ProcurationDeces obj){
        String sql =
                "insert procurationDeces set numero = '', "
                + " interesse =:interesse, fkPere =:fkPere, fkMere =:fkMere, fkDefunt =:fkDefunt,"
                + " divorceLe =:divorceLe, mandataire =:mandataire, "
                + " agentCreat =:agent, dateCreat = now(), statut = 1, fkCentre=:fkCentre, preuvePaiement=:preuvePaiement";
        
        try{
            if(this.executeSQLQuery(sql, 
                    new QueryParam("interesse", obj.getInteresse()),
                    new QueryParam("fkPere", obj.getFkPere()),
                    new QueryParam("fkMere", obj.getFkMere()),
                    new QueryParam("fkDefunt", obj.getFkDefunt()),
                    new QueryParam("divorceLe", obj.getDivorceLe()),
                    new QueryParam("mandataire", obj.getMandataire()),
                    new QueryParam("agent", obj.getAgentCreat()),
                    new QueryParam("fkCentre", obj.getFkCentre()),
                    new QueryParam("preuvePaiement", obj.getPreuvePaiement())
                    )){
                
                sql = "select numero from procurationDeces where agentCreat=:agentCreat order by dateCreat desc limit 1";
                obj.setNumero((String) this.selectSQL(sql, new QueryParam("agentCreat", obj.getAgentCreat())).get(0));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return obj.getNumero();
    }
}
