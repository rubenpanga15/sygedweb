/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import entities.AttestationNaissance;
import entities.QueryParam;

/**
 *
 * @author AdolphTrinix
 */
public class DaoAttestation extends DaoGeneric{
    
    public String saveAttestationNaissance(AttestationNaissance obj){
        String numero = null;
        
        String sql =
                "insert attestationNaissance set numero = '', "
                + " lieuNaissance =:lieuNaissance, dateNaissance =:dateNaissance, fkMere =:fkMere, fkPere =:fkPere, fkEnfant =:fkEnfant,"
                + " agentCreat =:agent, dateCreat = now(), statut = 1, fkCentre=:fkCentre, preuvePaiement=:preuvePaiement";
        
        try{
            if(this.executeSQLQuery(sql, 
                    new QueryParam("lieuNaissance", obj.getLieuNaissance()),
                    new QueryParam("fkPere", obj.getFkPere()),
                    new QueryParam("fkMere", obj.getFkMere()),
                    new QueryParam("fkEnfant", obj.getFkEnfant()),
                    new QueryParam("dateNaissance", obj.getDateNaissance()),
                    new QueryParam("agent", obj.getAgentCreat()),
                    new QueryParam("preuvePaiement", obj.getPreuvePaiement()),
                    new QueryParam("fkCentre", obj.getFkCentre())
                    )){
                
                sql = "select numero from attestationNaissance where agentCreat=:agentCreat order by dateCreat desc limit 1";
                obj.setNumero((String) this.selectSQL(sql, new QueryParam("agentCreat", obj.getAgentCreat())).get(0));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return obj.getNumero();
    }
}
