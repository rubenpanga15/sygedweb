/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import entities.Documentprocedure;
import entities.Procedure;
import entities.QueryParam;
import java.util.List;

/**
 *
 * @author Trinix
 */
public class DaoConfiguration extends DaoGeneric {
    
    public List<Documentprocedure> getDocuments(Integer fkProcedure){
        List<Documentprocedure> list = null;
        
        String hql = "from DocumentProcedure where fkProcedure =:fk";
        
        list = this.selectHQL(hql, new QueryParam("fk", fkProcedure));
        
        return list;
    }
    
    public List<Procedure> getProcedures(){
        List<Procedure> list = null;
        
        String hql = "from Procedure";
        
        list = this.selectHQL(hql);
        
        return list;
    }
}
