/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import contracts.IDeclaration;
import controllers.DeclarationController;
import entities.Attestation;
import entities.AttestationNaissance;
import entities.Declaration;
import entities.ErrorResponse;
import entities.ProcurationNaissance;
import entities.User;
import entities.ValueDataException;
import entities.utilities.AttestationRequest;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author InfoSys Tech
 */
@WebServlet("/api/save/attestation")
public class SaveaAttestationApi extends HttpServlet {

    private static final String TAG = SaveaAttestationApi.class.getSimpleName();

    public static final String ATTESTATION_NAISSANCE = "ATTESTATION DE NAISSANCE";

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Logger.printLog(TAG, "--- start ---");
        HttpDataResponse httpdataresponse = new HttpDataResponse();

        try {
            String requestBody = (String) request.getAttribute("query");
            Logger.printLog(TAG, requestBody);
            AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);

            String type = null;
            JSONObject obj = null;
            Object procuration = null;

            try {
                JSONObject jsonQuery = new JSONObject(requestBody);

                if (jsonQuery.has("type")) {
                    type = jsonQuery.getString("type");
                }

                if (jsonQuery.has("attestation")) {
                    obj = jsonQuery.getJSONObject("attestation");
                }

            } catch (Exception e) {
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }

            User user = (User) request.getAttribute("user");

            IDeclaration controller = new DeclarationController();
            String numero = controller.createAttestation(processAttestationNaissance(obj), user.getId(), type);

            httpdataresponse.setResponse(numero);
            httpdataresponse.setError(controller.getError());

        } catch (ValueDataException e) {
            httpdataresponse.getError().setErrorCode(ErrorResponse.KO);
            httpdataresponse.getError().setErrorDescription(AppConst.ERROR_UNKNOW);

        } catch (Exception e) {
            e.printStackTrace();

            httpdataresponse.getError().setErrorCode(ErrorResponse.KO);
            httpdataresponse.getError().setErrorDescription(AppConst.ERROR_UNKNOW);

        } finally {
            Logger.printLog(TAG, httpdataresponse.getError().toString());

            HttpUtilities.prepareHttpResponse(response, httpdataresponse).flush();
        }

    }

    private AttestationNaissance processAttestationNaissance(JSONObject json) {
        AttestationNaissance procuration = new AttestationNaissance();

        try {
            if (json.has("fkPere")) {
                procuration.setFkPere(json.getInt("fkPere"));
            }
            if (json.has("fkMere")) {
                procuration.setFkMere(json.getInt("fkMere"));
            }
            if (json.has("fkEnfant")) {
                procuration.setFkEnfant(json.getInt("fkEnfant"));
            }
            if(json.has("preuvePaiement")){
                procuration.setPreuvePaiement(json.getString("preuvePaiement"));
                
            }
            if (json.has("dateNaissance")) {
                try {
                    String divorceLe = json.getString("dateNaissance");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    procuration.setDateNaissance(sdf.parse(divorceLe));
                } catch (Exception e) {
                }
            }
            if (json.has("lieuNaissance")) {
                procuration.setLieuNaissance(json.getString("lieuNaissance"));
            }
            if (json.has("fkCentre")) {
                procuration.setFkCentre(json.getString("fkCentre"));
            }
        } catch (Exception e) {
        }

        return procuration;
    }

}
