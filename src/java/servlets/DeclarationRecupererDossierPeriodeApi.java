/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import controllers.DeclarationController;
import entities.ErrorResponse;
import entities.ValueDataException;
import entities.utilities.PeriodeRequest;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author InfoSys Tech
 */

@WebServlet("/api/declaration/recuperer-dossier/periode")
public class DeclarationRecupererDossierPeriodeApi extends HttpServlet{
    private static final String TAG = DeclarationRecupererDossierPeriodeApi.class.getSimpleName();

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "--- start ---");
        
        HttpDataResponse httpdataresponse = new HttpDataResponse();
        
        
        try{
            String requestBody = (String) request.getAttribute("query") ;
            AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);
            Logger.printLog(TAG, requestBody);
            
            PeriodeRequest obj = new PeriodeRequest();
            String dateDebut = null, dateFin = null, fkCentre = null;
            
            
            try{
                  JSONObject json = new JSONObject(requestBody);
                          
                  if(json.has("dateDebut"))
                      dateDebut = json.getString("dateDebut");
                  if(json.has("dateFin"))
                      dateFin = json.getString("dateFin");
                  if(json.has("fkCentre"))
                      fkCentre = json.getString("fkCentre");
                  
            }catch(Exception e){
                e.printStackTrace();
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }
            
            DeclarationController controller = new DeclarationController();
            
            httpdataresponse.setResponse(controller.getDeclarationByPeriode(dateDebut, dateFin, fkCentre));
            httpdataresponse.setError(controller.getError());
                      
        }catch(ValueDataException e){
            
            httpdataresponse.getError().setErrorCode(ErrorResponse.KO);
            httpdataresponse.getError().setErrorDescription(e.getMessage());
        
        }catch(Exception e){
            e.printStackTrace();
            
            httpdataresponse.getError().setErrorCode(ErrorResponse.KO);
            httpdataresponse.getError().setErrorDescription(AppConst.ERROR_UNKNOW);
        
        }finally{
            
            Logger.printLog(TAG, httpdataresponse.getError().toString());
            
            HttpUtilities.prepareHttpResponse(response, httpdataresponse).flush();
        }  
    }
}
