/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import controllers.ConfigurationController;
import entities.ErrorResponse;
import entities.ValueDataException;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author ghost06
 */
@WebServlet(name = "ListProcedureServlet", urlPatterns = {"/api/configuration/list-procedure"})
public class ListProcedureServlet extends HttpServlet {
    private static String TAG = ListProcedureServlet.class.getSimpleName();
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "Debut");
        
        HttpDataResponse httpResponse = new HttpDataResponse();
        
        try{
            
            ConfigurationController ctrl = new ConfigurationController();
            
            httpResponse.setResponse(ctrl.getListProcedures());
            if(ctrl.getError().getErrorCode().equals(ErrorResponse.KO))
                throw new ValueDataException(ctrl.getError().getErrorDescription());
            
            httpResponse.setError(ctrl.getError());
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
        }finally{
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
    }
}
