/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import contracts.IDeclaration;
import controllers.DeclarationController;
import entities.ErrorResponse;
import entities.Procuration;
import entities.ProcurationDeces;
import entities.ProcurationNaissance;
import entities.User;
import entities.ValueDataException;
import entities.utilities.ProcurationRequest;
import entities.utilities.UpLoadFileForProcurationRequest;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import others.AppUtilities;
import others.Logger;
import static servlets.SaveDeclarationApi.TAG;
import utilities.AppConst;

/**
 *
 * @author InfoSys Tech
 */
@WebServlet("/api/procuration/save")
public class SaveProcurationApi extends HttpServlet {

    private static final String TAG = SaveProcurationApi.class.getSimpleName();

    public static final String PROCURATION_NAISSANCE = "PROCURATION DE NAISSANCE";
    public static final String PROCURATION_DECES = "PROCURATION DE DECES";

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Logger.printLog(TAG, "--- start ---");
        HttpDataResponse httpdataresponse = new HttpDataResponse();

        try {

            String requestBody = (String) request.getAttribute("query");
            AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);
            Logger.printLog(TAG, requestBody);

            String type = null; JSONObject obj = null; Object procuration = null;

            try {
                JSONObject jsonQuery = new JSONObject(requestBody);
                
                if(jsonQuery.has("type"))
                    type = jsonQuery.getString("type");
                
                if(jsonQuery.has("procuration"))
                    obj = jsonQuery.getJSONObject("procuration");
                
            } catch (Exception e) {
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }
            
            switch(type){
                case PROCURATION_NAISSANCE:
                    procuration = processProcurationNaissance(obj);
                    break;
                case PROCURATION_DECES:
                    procuration = processProcurationDeces(obj);
                    break;
                default:
                    throw new ValueDataException("DOCUMENT NON-PRISE EN CHARGE");
            }

            User user = (User) request.getAttribute("user");

            IDeclaration controller = new DeclarationController();
            String numero = controller.createProcuration(procuration, user.getId(), type);

            httpdataresponse.setResponse(numero);
            httpdataresponse.setError(controller.getError());

        } catch (ValueDataException e) {
            httpdataresponse.getError().setErrorCode(ErrorResponse.KO);
            httpdataresponse.getError().setErrorDescription(AppConst.ERROR_UNKNOW);

        } catch (Exception e) {
            e.printStackTrace();

            httpdataresponse.getError().setErrorCode(ErrorResponse.KO);
            httpdataresponse.getError().setErrorDescription(AppConst.ERROR_UNKNOW);

        } finally {
            Logger.printLog(TAG, httpdataresponse.getError().toString());

            HttpUtilities.prepareHttpResponse(response, httpdataresponse).flush();

        }

    }

    private ProcurationNaissance processProcurationNaissance(JSONObject json) {
        ProcurationNaissance procuration = new ProcurationNaissance();

        try {
            if (json.has("interesse")) {
                procuration.setInteresse(json.getInt("interesse"));
            }
            if (json.has("fkPere")) {
                procuration.setFkPere(json.getInt("fkPere"));
            }
            if (json.has("fkMere")) {
                procuration.setFkMere(json.getInt("fkMere"));
            }
            if (json.has("fkEnfant")) {
                procuration.setFkEnfant(json.getInt("fkEnfant"));
            }
            if (json.has("situationMatrimoniale")) {
                procuration.setSituationMatrimoniale(json.getString("situationMatrimoniale"));
            }
            if (json.has("dateDivorce")) {
                try{
                    String divorceLe = json.getString("dateDivorce");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    procuration.setDivorceLe(sdf.parse(divorceLe));
                }catch(Exception e){}
            }
            
            if(json.has("mandataire")){
                procuration.setMandataire(json.getInt("mandataire"));
                
            }
            if(json.has("preuvePaiement")){
                procuration.setPreuvePaiement(json.getString("preuvePaiement"));
                
            }
            if(json.has("personneContact"))
                procuration.setPersonneContact(json.getInt("personneContact"));
            if(json.has("fkCentre"))
                procuration.setFkCentre(json.getString("fkCentre"));
        } catch (Exception e) {
        }

        return procuration;
    }
    
    private ProcurationDeces processProcurationDeces(JSONObject json) {
        ProcurationDeces procuration = new ProcurationDeces();

        try {
            if (json.has("interesse")) {
                procuration.setInteresse(json.getInt("interesse"));
            }
            if (json.has("fkPere")) {
                procuration.setFkPere(json.getInt("fkPere"));
            }
            if(json.has("preuvePaiement")){
                procuration.setPreuvePaiement(json.getString("preuvePaiement"));
                
            }
            if (json.has("fkMere")) {
                procuration.setFkMere(json.getInt("fkMere"));
            }
            if (json.has("fkDefunt")) {
                procuration.setFkDefunt(json.getInt("fkDefunt"));
            }
            if (json.has("dateDivorce")) {
                try{
                    String divorceLe = json.getString("dateDivorce");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    procuration.setDivorceLe(sdf.parse(divorceLe));
                }catch(Exception e){}
            }
            
            if(json.has("mandataire")){
                procuration.setMandataire(json.getInt("mandataire"));
                
            }
            if(json.has("fkCentre"))
                procuration.setFkCentre(json.getString("fkCentre"));
        } catch (Exception e) {
        }

        return procuration;
    }

}
