/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import contracts.IDeclaration;
import controllers.DeclarationController;
import entities.Declaration;
import entities.ErrorResponse;
import entities.User;
import entities.ValueDataException;
import entities.utilities.ActeDecesRequest;
import entities.utilities.ActeMariageRequest;
import entities.utilities.ActeNaissanceRequest;
import entities.utilities.DeclarationRequest;
import entities.utilities.DocumentRequest;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author InfoSys Tech
 */
@WebServlet("/api/save/declaration")
public class SaveDeclarationApi extends HttpServlet {

    public static final String TAG = SaveDeclarationApi.class.getSimpleName();

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Logger.printLog(TAG, "--- start ---");
        HttpDataResponse httpdataresponse = new HttpDataResponse();

        try {
            String requestBody = (String) request.getAttribute("query");
            AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);
            Logger.printLog(TAG, requestBody);
            
            User user = (User) request.getAttribute("user");

            DeclarationRequest obj = new DeclarationRequest();
            
            Object acte = null;
            
            try {
                JSONObject json = new JSONObject(requestBody);
                if (json.has("declaration")) {
                    JSONObject declaration = json.getJSONObject("declaration");
                    
                    if(declaration.has("beneficiaire"))
                        obj.setBeneficiaire(declaration.getInt("beneficiaire"));
                    
                    if(declaration.has("requerant"))
                        obj.setRequerant(declaration.getInt("requerant"));
                    
                    if(declaration.has("langue"))
                        obj.setLangue(declaration.getString("langue"));
                    
                    if(declaration.has("numeroRef"))
                        obj.setReferencePaiement(declaration.getString("numeroRef"));
                    
                    if(declaration.has("interprete"))
                        obj.setInterprete(declaration.getInt("interprete"));
                    
                    if(declaration.has("redacteur"))
                        obj.setRedacteur(declaration.getInt("redacteur"));
                    
                    obj.setRedacteur(user.getId());
                    
                    if(declaration.has("observationRedacteur"))
                        obj.setObservationRedacteur(declaration.getString("observationRedacteur"));
                    
                    if(declaration.has("type"))
                        obj.setType(declaration.getString("type"));
                    
                    if(declaration.has("fkCentre"))
                        obj.setCentre(declaration.getString("fkCentre"));
                    
                    if(declaration.has("documents"))
                        obj.setDocuments(this.parseDocumentRequest(declaration.getJSONArray("documents")));
                    
                    switch(obj.getType().toUpperCase()){
                        case Declaration.TYPE_ACTE_NAISSANCE :
                            acte = this.parseActeNaissance(json.getJSONObject("acte"));
                            obj.setBeneficiaire(json.getJSONObject("acte").getInt("beneficiaire"));
                            break;
                        case Declaration.TYPE_ACTE_DECES:
                            acte = this.parseActeDeces(json.getJSONObject("acte"));
                            obj.setBeneficiaire(json.getJSONObject("acte").getInt("beneficiaire"));
                            break;
                        case Declaration.TYPE_ACTE_MARIAGE:
                            acte = this.parseActeMariage(json.getJSONObject("acte"));
                            obj.setBeneficiaire(json.getJSONObject("acte").getInt("beneficiaire"));
                            break;
                        default :
                            throw new Exception("Type d'acte non supporté par le système");
                    }
                    
                }else{
                    throw new Exception();
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }
 
            IDeclaration controller = new DeclarationController();
            Declaration declaration = controller.createDeclation(obj.getCentre(), obj.getBeneficiaire(), obj.getRequerant(), obj.getLangue(), obj.getInterprete(), obj.getRedacteur(), obj.getObservationRedacteur(), obj.getType(), obj.getReferencePaiement(), user.getId(), acte, obj.getDocuments());

            httpdataresponse.setResponse(declaration.getNumero());
            httpdataresponse.setError(controller.getError());

        } catch (ValueDataException e) {
            httpdataresponse.getError().setErrorCode(ErrorResponse.KO);
            httpdataresponse.getError().setErrorDescription(AppConst.ERROR_UNKNOW);

        } catch (Exception e) {
            e.printStackTrace();

            httpdataresponse.getError().setErrorCode(ErrorResponse.KO);
            httpdataresponse.getError().setErrorDescription(AppConst.ERROR_UNKNOW);

        } finally {
            Logger.printLog(TAG, httpdataresponse.getError().toString());
            HttpUtilities.prepareHttpResponse(response, httpdataresponse).flush();
        }
    }
    
    private ActeNaissanceRequest parseActeNaissance(JSONObject json) throws JSONException{
        ActeNaissanceRequest obj = new ActeNaissanceRequest();
        
        if(json.has("numero"))
            obj.setNumero(json.getString("numero"));
        
        if(json.has("lieuNaissance"))
            obj.setLieuNaissance(json.getString("lieuNaissance"));
        
        if(json.has("dateNaissance"))
            obj.setDateNaissance(json.getString("dateNaissance"));
        
        if(json.has("pere"))
            obj.setPere(json.getInt("pere"));
        
        if(json.has("mere"))
            obj.setMere(json.getInt("mere"));
        
        return obj;
    }
    
    private ActeDecesRequest parseActeDeces(JSONObject json) throws JSONException{
        ActeDecesRequest obj = new ActeDecesRequest();
        
        if(json.has("numero"))
            obj.setNumero(json.getString("numero"));
        
        if(json.has("lieuDeces"))
            obj.setLieuDeces(json.getString("lieuDeces"));
        
        if(json.has("dateDeces"))
            obj.setDateDeces(json.getString("dateDeces"));
        
        if(json.has("pere"))
            obj.setPere(json.getInt("pere"));
        
        if(json.has("mere"))
            obj.setMere(json.getInt("mere"));
        if(json.has("circonstance"))
            obj.setCirconstance(json.getString("circonstance"));
        if(json.has("etatCivil"))
            obj.setEtatCivil(json.getString("etatCivil"));
        
        return obj;
    }
    
    private ActeMariageRequest parseActeMariage(JSONObject json) throws JSONException{
        ActeMariageRequest obj = new ActeMariageRequest();
        
        if(json.has("numero"))
            obj.setNumero(json.getString("numero"));
        
        if(json.has("lieuPublicite"))
            obj.setLieuPublicite(json.getString("lieuPublicite"));
        
        if(json.has("dateMariage"))
            obj.setDateMariage(json.getString("dateMariage"));
        
        if(json.has("pereEpoux"))
            obj.setPereEpoux(json.getInt("pereEpoux"));
        
        if(json.has("mereEpoux"))
            obj.setMereEpoux(json.getInt("mereEpoux"));
        if(json.has("pereEpouse"))
            obj.setPereEpouse(json.getInt("pereEpouse"));
        
        if(json.has("mereEpouse"))
            obj.setMereEpouse(json.getInt("mereEpouse"));
        
        if(json.has("regimeMatrimonial"))
            obj.setRegimeMatrimonial(json.getString("regimeMatrimonial"));
        
        return obj;
    }
    
    private List<DocumentRequest> parseDocumentRequest(JSONArray jsonArray) throws JSONException{
        List<DocumentRequest> list = new ArrayList();
        for(int i=0; i<jsonArray.length(); i++){
            JSONObject json = jsonArray.getJSONObject(i);
            
            DocumentRequest obj = new DocumentRequest();
            
            if(json.has("numeroRef"))
                obj.setNumeroRef(json.getString("numeroRef"));
            
            if(json.has("dateDocument"))
                obj.setDateDocument(json.getString("dateDocument"));
            
            if(json.has("typeDocument"))
                obj.setTypeDocument(json.getInt("typeDocument"));
            
            list.add(obj);
            
        }
        
        return list;
    }
}
