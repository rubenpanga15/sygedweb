/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import controllers.IdentificationController;
import entities.ErrorResponse;
import entities.Personne;
import entities.ValueDataException;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author Trinix
 */
@WebServlet(name = "IdentificationEditerPersonneApi", urlPatterns = {"/api/identification/edit-personne"})
public class IdentificationEditPersonneApi extends HttpServlet {
    private static final String TAG = IdentificationEditPersonneApi.class.getSimpleName();
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "Debut");
        HttpDataResponse httpResponse = new HttpDataResponse();
        
        try{
            String requestBody = HttpUtilities.getBodyRequest(request);
            Logger.printLog(TAG, requestBody);
            
            AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);
            
            Personne personne = null;
            
            try{
                personne = new Gson().fromJson(requestBody, new TypeToken<Personne>(){}.getType());
                
            }catch(Exception e){
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }
            
            IdentificationController ctrl = new IdentificationController();
            
            Integer id = ctrl.savePersonne(personne);
            
            httpResponse.setError(ctrl.getError());
            httpResponse.setResponse(id);
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode(ErrorResponse.KO);
            httpResponse.getError().setErrorDescription(e.getMessage());
            
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode(ErrorResponse.KO);
            httpResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
            
        }finally{
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
    }
}
