/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import contracts.IDeclaration;
import controllers.DeclarationController;
import entities.ErrorResponse;
import entities.User;
import entities.ValueDataException;
import entities.utilities.UploadFileForDeclarationRequestItem;
import entities.utilities.UploadFileForDeclarationResponse;
import entities.utilities.UplodeFileForDeclarationRequest;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author christianeboma
 */
@WebServlet("/api/archivage/upload-fichiers")
public class UploadFileForDeclaration extends HttpServlet {
    private static final String TAG = UploadFileForDeclaration.class.getSimpleName();
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "--- start ---");
        
        HttpDataResponse httpDataResponse = new HttpDataResponse();
        try{
            
            String requestBody = (String) request.getAttribute("query");
            AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);
            Logger.printLog(TAG, requestBody);
            
            UplodeFileForDeclarationRequest ufdr = null;
            
            try{
                ufdr = new Gson().fromJson(requestBody, new TypeToken<UplodeFileForDeclarationRequest>(){}.getType());
            }catch(Exception e){
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }
            
            IDeclaration controller = new DeclarationController();
            
            User user = (User) request.getAttribute("user");
            
            Logger.printLog(TAG, ufdr.getFiles().toString());
            
            List<UploadFileForDeclarationResponse> list = controller.saveDocumentLink(ufdr.getFiles(), user.getId());
            
            httpDataResponse.setError(controller.getError());
            httpDataResponse.setResponse(list);
            
        }catch(ValueDataException e){
            httpDataResponse.getError().setErrorCode(ErrorResponse.KO);
            httpDataResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            httpDataResponse.getError().setErrorCode(ErrorResponse.KO);
            httpDataResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
        }finally{
            Logger.printLog(TAG, httpDataResponse.getError().toString());
            
            HttpUtilities.prepareHttpResponse(response, httpDataResponse).flush();
        }
        
    }
}
