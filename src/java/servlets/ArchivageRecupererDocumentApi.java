/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import controllers.DeclarationController;
import entities.ErrorResponse;
import entities.ValueDataException;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author christianeboma
 */
@WebServlet("/api/archivage/recuperer-document")
public class ArchivageRecupererDocumentApi extends HttpServlet {
    private static final String TAG = ArchivageRecupererDocumentApi.class.getSimpleName();
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "--- start ---");
        
        HttpDataResponse httpDataResponse = new HttpDataResponse();
        try{
            String requestBody = (String) request.getAttribute("query");
            AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);
            Logger.printLog(TAG, requestBody);
            
            String typeActe = null;
            
            try{
                JSONObject json = new JSONObject(requestBody);
                
                if(json.has("type"))
                    typeActe = json.getString("type");
                
            }catch(Exception e){
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }
            
            DeclarationController controller = new DeclarationController();
            
            httpDataResponse.setResponse(controller.getDocumentAArchiver(typeActe));
            httpDataResponse.setError(controller.getError());
            
        }catch(ValueDataException e){
            httpDataResponse.getError().setErrorCode(ErrorResponse.KO);
            httpDataResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            httpDataResponse.getError().setErrorCode(ErrorResponse.KO);
            httpDataResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
        }finally{
            
            Logger.printLog(TAG, httpDataResponse.getError().toString());
            
            HttpUtilities.prepareHttpResponse(response, httpDataResponse).flush();
        }
    }
}
