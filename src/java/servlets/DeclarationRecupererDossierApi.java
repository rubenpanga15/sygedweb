/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import com.google.gson.Gson;
import contracts.IDeclaration;
import controllers.DeclarationController;
import entities.Declaration;
import entities.ErrorResponse;
import entities.ValueDataException;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author InfoSys Tech
 */
@WebServlet("/api/declaration/recuperer-dossier")
public class DeclarationRecupererDossierApi extends HttpServlet{    
    private static final String TAG = DeclarationRecupererDossierApi.class.getSimpleName();
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Logger.printLog(TAG, "--- start ---");
        HttpDataResponse httpDataResponse = new HttpDataResponse();
        
        try{
            String requestBody = (String) request.getAttribute("query");
            AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);
            Logger.printLog(TAG, requestBody);
            
            String numeroDossier = null;
            
            try{
                JSONObject json = new JSONObject(requestBody);
                
                if(json.has("numeroDossier"))
                    numeroDossier = json.getString("numeroDossier");
                
            }catch(Exception e){
                e.printStackTrace();
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }
            
            IDeclaration controller = new DeclarationController();
            Declaration declaration = controller.getDeclaration(numeroDossier);
            
            httpDataResponse.setResponse(declaration);
            httpDataResponse.setError(controller.getError());
            
        }catch(ValueDataException e){
            httpDataResponse.getError().setErrorCode(ErrorResponse.KO);
            httpDataResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            
            httpDataResponse.getError().setErrorCode(ErrorResponse.KO);
            httpDataResponse.getError().setErrorDescription(AppConst.ERROR_UNKNOW);
        }finally{   
            Logger.printLog(TAG, new Gson().toJson(httpDataResponse));
            
            HttpUtilities.prepareHttpResponse(response, httpDataResponse).flush();
        }
    }
    
    
}
