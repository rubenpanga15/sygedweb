/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import controllers.IdentificationController;
import entities.ErrorResponse;
import entities.Personne;
import entities.ValueDataException;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author Trinix
 */
@WebServlet(name = "SearchPersonneServlet", urlPatterns = {"/api/identification/search-person"})
public class SearchPersonneServlet extends HttpServlet {
    private static final String TAG = SearchPersonneServlet.class.getSimpleName();
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
        Logger.printLog(TAG, "Debut");
        HttpDataResponse httpResponse = new HttpDataResponse();
        
        try{
            String requestBody = (String)request.getAttribute("query");
            Logger.printLog(TAG, requestBody);
            
            AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);
            
            String motclef = null;
            
            try{
                JSONObject json = new JSONObject(requestBody);
                
                if(json.has("motclef"))
                    motclef = json.getString("motclef");
            }catch(Exception e){
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }
            
            IdentificationController ctrl = new IdentificationController();
            
            List<Personne> personnes = ctrl.searchPersonne(motclef);
            
            httpResponse.setError(ctrl.getError());
            httpResponse.setResponse(personnes);
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode(ErrorResponse.KO);
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            httpResponse.getError().setErrorCode(ErrorResponse.KO);
            httpResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
            
        }finally{
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
    }
}
