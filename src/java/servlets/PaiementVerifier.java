/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import controllers.ConfigurationController;
import controllers.DeclarationController;
import entities.Config;
import entities.ErrorResponse;
import entities.User;
import entities.ValueDataException;
import http.HttpDataResponse;
import http.HttpQuery;
import http.HttpUtilities;
import java.io.IOException;
import java.net.HttpURLConnection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author christianeboma
 */
@WebServlet("/api/paiement/verifier-paiement")
public class PaiementVerifier extends HttpServlet {
    private static final String TAG = PaiementVerifier.class.getSimpleName();
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "-- start --");
        HttpDataResponse httpDataResponse = new HttpDataResponse();
        
        try{
            String requestBody = (String) request.getAttribute("query");
            AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);
            Logger.printLog(TAG, requestBody);
            
            String numero = null;
            try{
                JSONObject json = new JSONObject(requestBody);
                
                if(json.has("numero"))
                    numero = json.getString("numero");
                
            }catch(Exception e){
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }
            
            DeclarationController controller = new DeclarationController();
            
            httpDataResponse.setResponse(controller.verifierPreuvePaiement(numero));
            httpDataResponse.setError(controller.getError());
            
        }catch(ValueDataException e){
            httpDataResponse.getError().setErrorCode(ErrorResponse.KO);
            httpDataResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            
            httpDataResponse.getError().setErrorCode(ErrorResponse.KO);
            httpDataResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
        }finally{
            HttpUtilities.prepareHttpResponse(response, httpDataResponse).flush();
        }
    }
}
