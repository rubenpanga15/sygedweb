/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import controllers.DeclarationController;
import entities.ErrorResponse;
import entities.User;
import entities.ValueDataException;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author InfoSys Tech
 */
@WebServlet("/api/declaration/valider")
public class DeclarationValiderAvisApi extends HttpServlet {
    private static final String TAG = DeclarationValiderAvisApi.class.getSimpleName();
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "--- start ---");
        HttpDataResponse httpdataresponse = new HttpDataResponse();
       
        try{
            
             String requestBody = (String) request.getAttribute("query");
             AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);
             Logger.printLog(TAG, requestBody);
             
             
             String avis = null , observation = null, numeroDeclaration = null; 
             Integer token = null;
             String typeAvis = null;
             
             try{
                 JSONObject json = new JSONObject(requestBody);
                 if(json.has("avis"))
                     avis = json.getString("avis");
                 if(json.has("observation"))
                     observation = json.getString("observation");
                 if(json.has("numeroDeclaration"))
                     numeroDeclaration = json.getString("numeroDeclaration");
             
             }catch(Exception e){
                 
                 e.printStackTrace();
                 throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
             }
             
             DeclarationController controller = new DeclarationController();
             
             User user = (User) request.getAttribute("user");
             
             httpdataresponse.setResponse(controller.avisVerification(numeroDeclaration, avis, observation, user.getId()));
             httpdataresponse.setError(controller.getError());
             
        
        }catch(ValueDataException e){
            
            httpdataresponse.getError().setErrorCode(ErrorResponse.KO);
            httpdataresponse.getError().setErrorDescription(AppConst.ERROR_UNKNOW);
        
        }catch(Exception e){
            
            e.printStackTrace();
            
            httpdataresponse.getError().setErrorCode(ErrorResponse.KO);
            httpdataresponse.getError().setErrorDescription(AppConst.ERROR_UNKNOW);
        
        }finally{
            Logger.printLog(TAG, httpdataresponse.getError().toString());
            HttpUtilities.prepareHttpResponse(response, httpdataresponse).flush();
        }
    }        
}
