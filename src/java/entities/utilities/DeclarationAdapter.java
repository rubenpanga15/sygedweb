/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.utilities;

import entities.Personne;
import java.util.Date;

/**
 *
 * @author AdolphTrinix
 */
public class DeclarationAdapter {
    private String numero;
     private Personne fkBeneficiaire;
     private Personne fkRequerant;
     private String langue;
     private Personne fkInterprete;
     private Personne fkRedacteur;
     private String observationRedacteur;
     private Personne fkPrepose;
     private String observationPrepose;
     private Personne fkOec;
     private String observationOec;
     private String avis;
     private String type;
     private Date dateAvis;
     private Boolean statut;
     private Date dateCreat;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Personne getFkBeneficiaire() {
        return fkBeneficiaire;
    }

    public void setFkBeneficiaire(Personne fkBeneficiaire) {
        this.fkBeneficiaire = fkBeneficiaire;
    }

    public Personne getFkRequerant() {
        return fkRequerant;
    }

    public void setFkRequerant(Personne fkRequerant) {
        this.fkRequerant = fkRequerant;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }

    public Personne getFkInterprete() {
        return fkInterprete;
    }

    public void setFkInterprete(Personne fkInterprete) {
        this.fkInterprete = fkInterprete;
    }

    public Personne getFkRedacteur() {
        return fkRedacteur;
    }

    public void setFkRedacteur(Personne fkRedacteur) {
        this.fkRedacteur = fkRedacteur;
    }

    public String getObservationRedacteur() {
        return observationRedacteur;
    }

    public void setObservationRedacteur(String observationRedacteur) {
        this.observationRedacteur = observationRedacteur;
    }

    public Personne getFkPrepose() {
        return fkPrepose;
    }

    public void setFkPrepose(Personne fkPrepose) {
        this.fkPrepose = fkPrepose;
    }

    public String getObservationPrepose() {
        return observationPrepose;
    }

    public void setObservationPrepose(String observationPrepose) {
        this.observationPrepose = observationPrepose;
    }

    public Personne getFkOec() {
        return fkOec;
    }

    public void setFkOec(Personne fkOec) {
        this.fkOec = fkOec;
    }

    public String getObservationOec() {
        return observationOec;
    }

    public void setObservationOec(String observationOec) {
        this.observationOec = observationOec;
    }

    public String getAvis() {
        return avis;
    }

    public void setAvis(String avis) {
        this.avis = avis;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDateAvis() {
        return dateAvis;
    }

    public void setDateAvis(Date dateAvis) {
        this.dateAvis = dateAvis;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }
     
     
}
