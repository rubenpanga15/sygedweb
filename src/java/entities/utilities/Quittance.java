/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.utilities;

import java.util.Date;

/**
 *
 * @author christianeboma
 */
public class Quittance {
    private String numero;
    private Boolean status;
    private Boolean used;
    private Date dateUsed;

    public Boolean getStatus() {
        if(status==null)
            status = false;
        
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getUsed() {
        if(used==null)
            used = true;
        
        return used;
    }

    public void setUsed(Boolean used) {
        this.used = used;
    }

    public Date getDateUsed() {
        return dateUsed;
    }

    public void setDateUsed(Date dateUsed) {
        this.dateUsed = dateUsed;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
