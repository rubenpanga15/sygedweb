/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.utilities;

import java.util.List;

/**
 *
 * @author InfoSys Tech
 */
public class UpLoadFileForProcurationRequest {
    private String username;
    private String token;
    private List<UpLoadFileForProcurationRequest> files;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<UpLoadFileForProcurationRequest> getFiles() {
        return files;
    }

    public void setFiles(List<UpLoadFileForProcurationRequest> files) {
        this.files = files;
    }
    
    
}
