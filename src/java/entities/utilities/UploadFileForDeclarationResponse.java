/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.utilities;

import java.util.List;

/**
 *
 * @author christianeboma
 */
public class UploadFileForDeclarationResponse {
    private String numeroDeclaration;
    private int typeDocument;
    private List<Integer> filesId;

    public int getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(int typeDocument) {
        this.typeDocument = typeDocument;
    }

    public List<Integer> getFilesId() {
        return filesId;
    }

    public void setFilesId(List<Integer> filesId) {
        this.filesId = filesId;
    }

    public String getNumeroDeclaration() {
        return numeroDeclaration;
    }

    public void setNumeroDeclaration(String numeroDeclaration) {
        this.numeroDeclaration = numeroDeclaration;
    }
}
