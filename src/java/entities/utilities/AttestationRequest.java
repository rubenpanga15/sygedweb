package entities.utilities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Date;

/**
 *
 * @author InfoSys Tech
 */
public class AttestationRequest {
    private String username;
    private String token;
    private String type;
    private Object attestation;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getAttestation() {
        return attestation;
    }

    public void setAttestation(Object attestation) {
        this.attestation = attestation;
    }

    
    
    
}
