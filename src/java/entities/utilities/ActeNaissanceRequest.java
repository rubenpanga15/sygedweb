/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.utilities;

import java.util.Date;

/**
 *
 * @author christianeboma
 */
public class ActeNaissanceRequest {

    private String numero;
    private String lieuNaissance;
    private String dateNaissance;
    private Integer pere;
    private Integer mere;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public Integer getPere() {
        return pere;
    }

    public void setPere(Integer pere) {
        this.pere = pere;
    }

    public Integer getMere() {
        return mere;
    }

    public void setMere(Integer mere) {
        this.mere = mere;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
    
    
}
