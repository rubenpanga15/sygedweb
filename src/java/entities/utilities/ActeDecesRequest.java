/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.utilities;

/**
 *
 * @author rubenpanga
 */
public class ActeDecesRequest {
    
    private String numero;
    private Integer pere;
    private Integer mere;
    private String dateDeces;
    private String lieuDeces;
    private String circonstance;
    private String etatCivil;

    public String getEtatCivil() {
        return etatCivil;
    }

    public void setEtatCivil(String etatCivil) {
        this.etatCivil = etatCivil;
    }
    
    

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getPere() {
        return pere;
    }

    public void setPere(Integer pere) {
        this.pere = pere;
    }

    public Integer getMere() {
        return mere;
    }

    public void setMere(Integer mere) {
        this.mere = mere;
    }

    public String getDateDeces() {
        return dateDeces;
    }

    public void setDateDeces(String dateDeces) {
        this.dateDeces = dateDeces;
    }

    public String getLieuDeces() {
        return lieuDeces;
    }

    public void setLieuDeces(String lieuDeces) {
        this.lieuDeces = lieuDeces;
    }

    public String getCirconstance() {
        return circonstance;
    }

    public void setCirconstance(String circonstance) {
        this.circonstance = circonstance;
    }
    
    
    
}
