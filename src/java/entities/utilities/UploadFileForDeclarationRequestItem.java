/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.utilities;

import java.util.List;

/**
 *
 * @author christianeboma
 */
public class UploadFileForDeclarationRequestItem {
    private List<String> files;
    private String numeroDeclaration;
    private Integer typeDocument;

    public List<String> getFiles() {
        return files;
    }

    public void setFiles(List<String> files) {
        this.files = files;
    }

    public String getNumeroDeclaration() {
        return numeroDeclaration;
    }

    public void setNumeroDeclaration(String numeroDeclaration) {
        this.numeroDeclaration = numeroDeclaration;
    }

    public Integer getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(Integer typeDocument) {
        this.typeDocument = typeDocument;
    }

    @Override
    public String toString() {
        return "UploadFileForDeclarationRequestItem{" + "files=" + files + ", numeroDeclaration=" + numeroDeclaration + ", typeDocument=" + typeDocument + '}';
    }
    
    
    
    
}
