/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.utilities;

import java.util.List;

/**
 *
 * @author InfoSys Tech
 */
public class UpLoadFileForProcurationRequestItem {
    private String dateProcuration;
    private List<String> files;
    private String userId ;

    public String getDateProcuration() {
        return dateProcuration;
    }

    public void setDateProcuration(String dateProcuration) {
        this.dateProcuration = dateProcuration;
    }

    public List<String> getFiles() {
        return files;
    }

    public void setFiles(List<String> files) {
        this.files = files;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    
}
