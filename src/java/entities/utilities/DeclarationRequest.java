/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.utilities;

import java.util.List;

/**
 *
 * @author InfoSys Tech
 */
public class DeclarationRequest {

    private Integer beneficiaire;
    private Integer requerant;
    private String langue;
    private String centre;
    private Integer interprete;
    private Integer redacteur;
    private String observationRedacteur;
    private String type;
    private String referencePaiement;
    
    private List<DocumentRequest> documents;

    public Integer getBeneficiaire() {
        return beneficiaire;
    }

    public void setBeneficiaire(Integer beneficiaire) {
        this.beneficiaire = beneficiaire;
    }

    public Integer getRequerant() {
        return requerant;
    }

    public void setRequerant(Integer requerant) {
        this.requerant = requerant;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }

    public Integer getInterprete() {
        return interprete;
    }

    public void setInterprete(Integer interprete) {
        this.interprete = interprete;
    }

    public Integer getRedacteur() {
        return redacteur;
    }

    public void setRedacteur(Integer redacteur) {
        this.redacteur = redacteur;
    }

    public String getObservationRedacteur() {
        return observationRedacteur;
    }

    public void setObservationRedacteur(String observationRedacteur) {
        this.observationRedacteur = observationRedacteur;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCentre() {
        return centre;
    }

    public void setCentre(String centre) {
        this.centre = centre;
    }

    public List<DocumentRequest> getDocuments() {
        return documents;
    }

    public void setDocuments(List<DocumentRequest> documents) {
        this.documents = documents;
    }

    public String getReferencePaiement() {
        return referencePaiement;
    }

    public void setReferencePaiement(String referencePaiement) {
        this.referencePaiement = referencePaiement;
    }

    
}
