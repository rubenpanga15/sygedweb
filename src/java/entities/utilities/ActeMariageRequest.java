/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.utilities;

/**
 *
 * @author rubenpanga
 */
public class ActeMariageRequest {
    
    private Integer pereEpoux;
    private Integer mereEpoux;
    private Integer pereEpouse;
    private Integer mereEpouse;
    private String lieuPublicite;
    private String regimeMatrimonial;
    private String dateMariage;
    private String numero;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
    
    

    public Integer getPereEpoux() {
        return pereEpoux;
    }

    public void setPereEpoux(Integer pereEpoux) {
        this.pereEpoux = pereEpoux;
    }

    public Integer getMereEpoux() {
        return mereEpoux;
    }

    public void setMereEpoux(Integer mereEpoux) {
        this.mereEpoux = mereEpoux;
    }

    public Integer getPereEpouse() {
        return pereEpouse;
    }

    public void setPereEpouse(Integer pereEpouse) {
        this.pereEpouse = pereEpouse;
    }

    public Integer getMereEpouse() {
        return mereEpouse;
    }

    public void setMereEpouse(Integer mereEpouse) {
        this.mereEpouse = mereEpouse;
    }

    public String getLieuPublicite() {
        return lieuPublicite;
    }

    public void setLieuPublicite(String lieuPublicite) {
        this.lieuPublicite = lieuPublicite;
    }

    public String getRegimeMatrimonial() {
        return regimeMatrimonial;
    }

    public void setRegimeMatrimonial(String regimeMatrimonial) {
        this.regimeMatrimonial = regimeMatrimonial;
    }

    public String getDateMariage() {
        return dateMariage;
    }

    public void setDateMariage(String dateMariage) {
        this.dateMariage = dateMariage;
    }
    
    
    
}
