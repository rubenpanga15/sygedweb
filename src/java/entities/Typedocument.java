package entities;
// Generated 4 ao�t 2020 10:41:04 by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Typedocument generated by hbm2java
 */
public class Typedocument  implements java.io.Serializable {


     private Integer id;
     private String description;
     private Boolean ascanner;
     private Boolean statut;
     private Date dateCreat;

    public Typedocument() {
    }

    public Typedocument(String description, boolean ascanner, boolean statut, Date dateCreat) {
       this.description = description;
       this.ascanner = ascanner;
       this.statut = statut;
       this.dateCreat = dateCreat;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    public boolean isAscanner() {
        return this.ascanner;
    }
    
    public void setAscanner(boolean ascanner) {
        this.ascanner = ascanner;
    }
    public boolean isStatut() {
        return this.statut;
    }
    
    public void setStatut(boolean statut) {
        this.statut = statut;
    }
    public Date getDateCreat() {
        return this.dateCreat;
    }
    
    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }




}


