package entities;
// Generated 6 aout 2020 12:09:01 by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Centre generated by hbm2java
 */
public class Centre  implements java.io.Serializable {


     private String code;
     private String description;
     private boolean statut;
     private Date dateCreat;

    public Centre() {
    }

    public Centre(String code, String description, boolean statut, Date dateCreat) {
       this.code = code;
       this.description = description;
       this.statut = statut;
       this.dateCreat = dateCreat;
    }
   
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    public boolean isStatut() {
        return this.statut;
    }
    
    public void setStatut(boolean statut) {
        this.statut = statut;
    }
    public Date getDateCreat() {
        return this.dateCreat;
    }
    
    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }




}


