/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author InfoSys Tech
 */
public class Attestation {
    private String numero;
    private String lieuNaissance;
    private Date dateNaissance;
    private Integer fkPere;
    private Integer fkMere;
    private String fkDeclaration;
    private Boolean statut;
    private Date dateCreat;
    private Integer agentCreat;
    private String fkCentre;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public Integer getFkPere() {
        return fkPere;
    }

    public void setFkPere(Integer fkPere) {
        this.fkPere = fkPere;
    }

    public Integer getFkMere() {
        return fkMere;
    }

    public void setFkMere(Integer fkMere) {
        this.fkMere = fkMere;
    }

    public String getFkDeclaration() {
        return fkDeclaration;
    }

    public void setFkDeclaration(String fkDeclaration) {
        this.fkDeclaration = fkDeclaration;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Integer getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Integer agentCreat) {
        this.agentCreat = agentCreat;
    }

    public String getFkCentre() {
        return fkCentre;
    }

    public void setFkCentre(String fkCentre) {
        this.fkCentre = fkCentre;
    }
    
    
    
}
