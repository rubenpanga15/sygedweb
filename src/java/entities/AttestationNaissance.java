/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author rubenpanga
 */
public class AttestationNaissance {
    
    private String numero;
    private String lieuNaissance;
    private Date dateNaissance;
    private Integer fkPere;
    private Integer fkMere;
    private Integer fkEnfant;
    private String fkCentre;
    private Integer agentCreat;
    private Boolean statut;
    private Date dateCreat;
    
    private Boolean isImprimer;
    private Integer agentImpression;
    private Date dateImpression;
    
    private String preuvePaiement;

    public String getPreuvePaiement() {
        return preuvePaiement;
    }

    public void setPreuvePaiement(String preuvePaiement) {
        this.preuvePaiement = preuvePaiement;
    }

    public Boolean getIsImprimer() {
        return isImprimer;
    }

    public void setIsImprimer(Boolean isImprimer) {
        this.isImprimer = isImprimer;
    }

    public Integer getAgentImpression() {
        return agentImpression;
    }

    public void setAgentImpression(Integer agentImpression) {
        this.agentImpression = agentImpression;
    }

    public Date getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(Date dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public Integer getFkPere() {
        return fkPere;
    }

    public void setFkPere(Integer fkPere) {
        this.fkPere = fkPere;
    }

    public Integer getFkMere() {
        return fkMere;
    }

    public void setFkMere(Integer fkMere) {
        this.fkMere = fkMere;
    }

    public String getFkCentre() {
        return fkCentre;
    }

    public void setFkCentre(String fkCentre) {
        this.fkCentre = fkCentre;
    }

    public Integer getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Integer agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Integer getFkEnfant() {
        return fkEnfant;
    }

    public void setFkEnfant(Integer fkEnfant) {
        this.fkEnfant = fkEnfant;
    }
    
}
