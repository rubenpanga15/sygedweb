/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author InfoSys Tech
 */
public class Procuration {
    private String numero;
    private String fkPere;
    private String interesse;
    private String fkMere;
    private String enfant;
    private String situationMatrimoniale;
    private String divorceLe;
    private String mendataire;
    private Boolean statut;
    private Date dateCreat;
    private String personneContact;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getFkPere() {
        return fkPere;
    }

    public void setFkPere(String fkPere) {
        this.fkPere = fkPere;
    }

    public String getInteresse() {
        return interesse;
    }

    public void setInteresse(String interesse) {
        this.interesse = interesse;
    }

    public String getFkMere() {
        return fkMere;
    }

    public void setFkMere(String fkMere) {
        this.fkMere = fkMere;
    }

    public String getEnfant() {
        return enfant;
    }

    public void setEnfant(String enfant) {
        this.enfant = enfant;
    }

    public String getSituationMatrimoniale() {
        return situationMatrimoniale;
    }

    public void setSituationMatrimoniale(String situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
    }

    public String getDivorceLe() {
        return divorceLe;
    }

    public void setDivorceLe(String divorceLe) {
        this.divorceLe = divorceLe;
    }

    public String getMendataire() {
        return mendataire;
    }

    public void setMendataire(String mendataire) {
        this.mendataire = mendataire;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getPersonneContact() {
        return personneContact;
    }

    public void setPersonneContact(String personneContact) {
        this.personneContact = personneContact;
    }

   
}
