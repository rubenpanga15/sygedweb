/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author rubenpanga
 */
public class ProcurationDeces {
    
    private String numero;
    private Integer interesse;
    private Integer fkPere;
    private Integer fkMere;
    private Integer fkDefunt;
    private Date divorceLe;
    private Integer mandataire;
    private Integer agentCreat;
    private Date dateCreat;
    private Boolean statut;
    private String fkCentre;
    
    private Boolean isImprimer;
    private Integer agentImpression;
    private Date dateImpression;
    
    private String preuvePaiement;

    public String getPreuvePaiement() {
        return preuvePaiement;
    }

    public void setPreuvePaiement(String preuvePaiement) {
        this.preuvePaiement = preuvePaiement;
    }
    
    

    public Boolean getIsImprimer() {
        return isImprimer;
    }

    public void setIsImprimer(Boolean isImprimer) {
        this.isImprimer = isImprimer;
    }

    public Integer getAgentImpression() {
        return agentImpression;
    }

    public void setAgentImpression(Integer agentImpression) {
        this.agentImpression = agentImpression;
    }

    public Date getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(Date dateImpression) {
        this.dateImpression = dateImpression;
    }
    
    

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getInteresse() {
        return interesse;
    }

    public void setInteresse(Integer interesse) {
        this.interesse = interesse;
    }

    public Integer getFkPere() {
        return fkPere;
    }

    public void setFkPere(Integer fkPere) {
        this.fkPere = fkPere;
    }

    public Integer getFkMere() {
        return fkMere;
    }

    public void setFkMere(Integer fkMere) {
        this.fkMere = fkMere;
    }

    public Integer getFkDefunt() {
        return fkDefunt;
    }

    public void setFkDefunt(Integer fkDefunt) {
        this.fkDefunt = fkDefunt;
    }

    public Date getDivorceLe() {
        return divorceLe;
    }

    public void setDivorceLe(Date divorceLe) {
        this.divorceLe = divorceLe;
    }

    public Integer getMandataire() {
        return mandataire;
    }

    public void setMandataire(Integer mandataire) {
        this.mandataire = mandataire;
    }

    public Integer getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Integer agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public String getFkCentre() {
        return fkCentre;
    }

    public void setFkCentre(String fkCentre) {
        this.fkCentre = fkCentre;
    }
    
    
    
}
