/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author rubenpanga
 */
public class ProcurationNaissance {
    private String numero;
    private Integer interesse;
    private Integer fkPere;
    private Integer fkMere;
    private Integer fkEnfant;
    private String situationMatrimoniale;
    private Date divorceLe;
    private Integer mandataire;
    private Integer personneContact;
    private Integer agentCreat;
    private Date dateCreat;
    private Boolean statut;
    private String fkCentre;
    private Boolean isImprimer;
    private Integer agentImpression;
    private Date dateImpression;
    
    private String preuvePaiement;
    
    private Personne interessePersonne;
    private Personne pere;
    private Personne mere;
    private Personne enfant;
    private Personne mandatairePersonne;
    private Personne personneContactPersonne;
    private User agent;

    public String getPreuvePaiement() {
        return preuvePaiement;
    }

    public void setPreuvePaiement(String preuvePaiement) {
        this.preuvePaiement = preuvePaiement;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getInteresse() {
        return interesse;
    }

    public void setInteresse(Integer interesse) {
        this.interesse = interesse;
    }

    public Integer getFkPere() {
        return fkPere;
    }

    public void setFkPere(Integer fkPere) {
        this.fkPere = fkPere;
    }

    public Integer getFkMere() {
        return fkMere;
    }

    public void setFkMere(Integer fkMere) {
        this.fkMere = fkMere;
    }

    public Integer getFkEnfant() {
        return fkEnfant;
    }

    public void setFkEnfant(Integer fkEnfant) {
        this.fkEnfant = fkEnfant;
    }

    public String getSituationMatrimoniale() {
        return situationMatrimoniale;
    }

    public void setSituationMatrimoniale(String situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
    }

    public Date getDivorceLe() {
        return divorceLe;
    }

    public void setDivorceLe(Date divorceLe) {
        this.divorceLe = divorceLe;
    }

    public Integer getMandataire() {
        return mandataire;
    }

    public void setMandataire(Integer mandataire) {
        this.mandataire = mandataire;
    }

    public Integer getPersonneContact() {
        return personneContact;
    }

    public void setPersonneContact(Integer personneContact) {
        this.personneContact = personneContact;
    }

    public Integer getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Integer agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public String getFkCentre() {
        return fkCentre;
    }

    public void setFkCentre(String fkCentre) {
        this.fkCentre = fkCentre;
    }

    public Personne getInteressePersonne() {
        return interessePersonne;
    }

    public void setInteressePersonne(Personne interessePersonne) {
        this.interessePersonne = interessePersonne;
    }

    public Personne getPere() {
        return pere;
    }

    public void setPere(Personne pere) {
        this.pere = pere;
    }

    public Personne getMere() {
        return mere;
    }

    public void setMere(Personne mere) {
        this.mere = mere;
    }

    public Personne getEnfant() {
        return enfant;
    }

    public void setEnfant(Personne enfant) {
        this.enfant = enfant;
    }

    public Personne getMandatairePersonne() {
        return mandatairePersonne;
    }

    public void setMandatairePersonne(Personne mandatairePersonne) {
        this.mandatairePersonne = mandatairePersonne;
    }

    public Personne getPersonneContactPersonne() {
        return personneContactPersonne;
    }

    public void setPersonneContactPersonne(Personne personneContactPersonne) {
        this.personneContactPersonne = personneContactPersonne;
    }

    public User getAgent() {
        return agent;
    }

    public void setAgent(User agent) {
        this.agent = agent;
    }

    public Boolean getIsImprimer() {
        return isImprimer;
    }

    public void setIsImprimer(Boolean isImprimer) {
        this.isImprimer = isImprimer;
    }

    public Integer getAgentImpression() {
        return agentImpression;
    }

    public void setAgentImpression(Integer agentImpression) {
        this.agentImpression = agentImpression;
    }

    public Date getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(Date dateImpression) {
        this.dateImpression = dateImpression;
    }
    
    
}
