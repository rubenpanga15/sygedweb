package entities;
// Generated 4 ao�t 2020 10:41:04 by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Personne generated by hbm2java
 */
public class Personne  implements java.io.Serializable {


     private Integer id;
     private String nom;
     private String postNom;
     private String prenom;
     private String lieuNaissance;
     private Date dateNaissance;
     private String sexe;
     private String telephone;
     private String email;
     private String profession;
     private String etatCivil;
     private String nomPere;
     private String nomMere;
     private String province;
     private String district;
     private String secteur;
     private String groupement;
     private String adresse;
     private Boolean statut;
     private Date dateCreat;

    public Personne() {
    }

	
    public Personne(String nom, String prenom, String lieuNaissance, Date dateNaissance, String sexe, String profession, String nomPere, String nomMere, String adresse, boolean statut, Date dateCreat) {
        this.nom = nom;
        this.prenom = prenom;
        this.lieuNaissance = lieuNaissance;
        this.dateNaissance = dateNaissance;
        this.sexe = sexe;
        this.profession = profession;
        this.nomPere = nomPere;
        this.nomMere = nomMere;
        this.adresse = adresse;
        this.statut = statut;
        this.dateCreat = dateCreat;
    }
    public Personne(String nom, String postNom, String prenom, String lieuNaissance, Date dateNaissance, String sexe, String telephone, String email, String profession, String etatCivil, String nomPere, String nomMere, String province, String district, String secteur, String groupement, String adresse, boolean statut, Date dateCreat) {
       this.nom = nom;
       this.postNom = postNom;
       this.prenom = prenom;
       this.lieuNaissance = lieuNaissance;
       this.dateNaissance = dateNaissance;
       this.sexe = sexe;
       this.telephone = telephone;
       this.email = email;
       this.profession = profession;
       this.etatCivil = etatCivil;
       this.nomPere = nomPere;
       this.nomMere = nomMere;
       this.province = province;
       this.district = district;
       this.secteur = secteur;
       this.groupement = groupement;
       this.adresse = adresse;
       this.statut = statut;
       this.dateCreat = dateCreat;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public String getNom() {
        return this.nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getPostNom() {
        return this.postNom;
    }
    
    public void setPostNom(String postNom) {
        this.postNom = postNom;
    }
    public String getPrenom() {
        return this.prenom;
    }
    
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public String getLieuNaissance() {
        return this.lieuNaissance;
    }
    
    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }
    public Date getDateNaissance() {
        return this.dateNaissance;
    }
    
    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
    public String getSexe() {
        return this.sexe;
    }
    
    public void setSexe(String sexe) {
        this.sexe = sexe;
    }
    public String getTelephone() {
        return this.telephone;
    }
    
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    public String getProfession() {
        return this.profession;
    }
    
    public void setProfession(String profession) {
        this.profession = profession;
    }
    public String getEtatCivil() {
        return this.etatCivil;
    }
    
    public void setEtatCivil(String etatCivil) {
        this.etatCivil = etatCivil;
    }
    public String getNomPere() {
        return this.nomPere;
    }
    
    public void setNomPere(String nomPere) {
        this.nomPere = nomPere;
    }
    public String getNomMere() {
        return this.nomMere;
    }
    
    public void setNomMere(String nomMere) {
        this.nomMere = nomMere;
    }
    public String getProvince() {
        return this.province;
    }
    
    public void setProvince(String province) {
        this.province = province;
    }
    public String getDistrict() {
        return this.district;
    }
    
    public void setDistrict(String district) {
        this.district = district;
    }
    public String getSecteur() {
        return this.secteur;
    }
    
    public void setSecteur(String secteur) {
        this.secteur = secteur;
    }
    public String getGroupement() {
        return this.groupement;
    }
    
    public void setGroupement(String groupement) {
        this.groupement = groupement;
    }
    public String getAdresse() {
        return this.adresse;
    }
    
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    public boolean isStatut() {
        return this.statut;
    }
    
    public void setStatut(boolean statut) {
        this.statut = statut;
    }
    public Date getDateCreat() {
        return this.dateCreat;
    }
    
    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }




}


